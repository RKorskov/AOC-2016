// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-26 19:32:49 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  http://adventofcode.com/2016/day/18

  --- Day 18: Like a Rogue ---

  As you enter this room, you hear a loud click! Some of the tiles in
  the floor here seem to be pressure plates for traps, and the trap
  you just triggered has run out of... whatever it tried to do to
  you. You doubt you'll be so lucky next time.

  Upon closer examination, the traps and safe tiles in this room seem
  to follow a pattern. The tiles are arranged into rows that are all
  the same width; you take note of the safe tiles (.) and traps (^) in
  the first row (your puzzle input).

  The type of tile (trapped or safe) in each row is based on the types
  of the tiles in the same position, and to either side of that
  position, in the previous row. (If either side is off either end of
  the row, it counts as "safe" because there isn't a trap embedded in
  the wall.)

  For example, suppose you know the first row (with tiles marked by
  letters) and want to determine the next row (with tiles marked by
  numbers):

ABCDE
12345

  The type of tile 2 is based on the types of tiles A, B, and C; the
  type of tile 5 is based on tiles D, E, and an imaginary "safe"
  tile. Let's call these three tiles from the previous row the left,
  center, and right tiles, respectively. Then, a new tile is a trap
  only in one of the following situations:

    Its left and center tiles are traps, but its right tile is not.

    Its center and right tiles are traps, but its left tile is not.

    Only its left tile is a trap.

    Only its right tile is a trap.

  In any other situation, the new tile is safe.

  Then, starting with the row ..^^., you can determine the next row by
  applying those rules to each new tile:

    The leftmost character on the next row considers the left
    (nonexistent, so we assume "safe"), center (the first ., which
    means "safe"), and right (the second ., also "safe") tiles on the
    previous row. Because all of the trap rules require a trap in at
    least one of the previous three tiles, the first tile on this new
    row is also safe, ..

    The second character on the next row considers its left (.),
    center (.), and right (^) tiles from the previous row. This
    matches the fourth rule: only the right tile is a trap. Therefore,
    the next tile in this new row is a trap, ^.

    The third character considers .^^, which matches the second trap
    rule: its center and right tiles are traps, but its left tile is
    not. Therefore, this tile is also a trap, ^.

    The last two characters in this new row match the first and third
    rules, respectively, and so they are both also traps, ^.

  After these steps, we now know the next row of tiles in the room:
  .^^^^. Then, we continue on to the next row, using the same rules,
  and get ^^..^. After determining two new rows, our map looks like
  this:

..^^.
.^^^^
^^..^

  Here's a larger example with ten tiles per row and ten rows:

.^^.^.^^^^
^^^...^..^
^.^^.^.^^.
..^^...^^^
.^^^^.^^.^
^^..^.^^..
^^^^..^^^.
^..^^^^.^^
.^^^..^.^^
^^.^^^..^^

  In ten rows, this larger example has 38 safe tiles.

  Starting with the map in your puzzle input, in a total of 40 rows
  (including the starting row), how many safe tiles are there?

......^.^^.....^^^^^^^^^...^.^..^^.^^^..^.^..^.^^^.^^^^..^^.^.^.....^^^^^..^..^^^..^^.^.^..^^..^^^..

  --- Part Two ---

  How many safe tiles are there in a total of 400000 rows?
 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
//import java.lang.Integer;
//import java.lang.Long;
//import java.lang.Math;
import java.lang.String;
import java.lang.StringBuilder;
import java.security.MessageDigest;
//import java.text.DateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Scanner; // Scanner (System.in);
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class aoc_18 {

    public final static boolean __DEBUG__ = false;
    //public final static boolean __DEBUG__ = true;

    /*
    private static int max (final int a, final int b) { return a > b ? a : b; }

    private static int min (final int a, final int b) { return a < b ? a : b; }

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }
    */


    final static char CHR_TRAP = '^';
    final static char CHR_SAFE = '.';


    private static boolean is_safe (final char[] seed, final int x) {
        return (x >= 0 && x < seed.length) ? (seed[x] == CHR_SAFE) : true;
    }


    private static boolean rule1 (final char[] seed, final int x) {
        if (!is_safe (seed, x-1) && !is_safe (seed, x) && is_safe (seed, x+1))
            return true;
        return false;
    }


    private static boolean rule2 (final char[] seed, final int x) {
        if (is_safe (seed, x-1) && !is_safe (seed, x) && !is_safe (seed, x+1))
            return true;
        return false;
    }


    private static boolean rule3 (final char[] seed, final int x) {
        if (is_safe (seed, x-1) && is_safe (seed, x) && !is_safe (seed, x+1))
            return true;
        return false;
    }


    private static boolean rule4 (final char[] seed, final int x) {
        if (!is_safe (seed, x-1) && is_safe (seed, x) && is_safe (seed, x+1))
            return true;
        return false;
    }


    private static boolean is_new_trap (final char[] seed, final int x) {
        return rule1 (seed, x) || rule2 (seed, x)
            || rule3 (seed, x) || rule4 (seed, x);
    }


    private static char[] genline (final char[] seed) {
        char[] nl;
        int i;
        final int dl;
        dl = seed.length;
        nl = new char [dl];
        for (i = 0; i < nl.length; ++i)
            nl[i] = is_new_trap (seed, i) ? CHR_TRAP : CHR_SAFE;
        return nl;
    }


    private static int safe_count (final char[] tline) {
        int i, cnt;
        for (cnt = i = 0; i < tline.length; ++i)
            if (is_safe (tline, i))
                ++cnt;
        return cnt;
    }


    public static void main (String[] args) throws Exception {
        final String seed;
        final int rep;
        int i, cnt;
        seed = args.length > 0 ? args[0] : "......^.^^.....^^^^^^^^^...^.^..^^.^^^..^.^..^.^^^.^^^^..^^.^.^.....^^^^^..^..^^^..^^.^.^..^^..^^^..";
        rep = args.length > 1 ? Integer.parseUnsignedInt (args[1]) : 40;
        //String[] cmds = read_input(); if (cmds == null) return;
        char[] tnl;
        //System.out.println (seed);
        for (tnl = seed.toCharArray(), i = 1, cnt = safe_count (tnl);
             i < rep; ++i) {
            tnl = genline (tnl);
            cnt += safe_count (tnl);
            //System.out.println (new String (tnl));
        }
        System.out.println (cnt);
    }
}

