// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-15 17:11:04 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  --- Day 7: Internet Protocol Version 7 ---

  While snooping around the local network of EBHQ, you compile a list
  of IP addresses (they're IPv7, of course; IPv6 is much too
  limited). You'd like to figure out which IPs support TLS
  (transport-layer snooping).

  An IP supports TLS if it has an Autonomous Bridge Bypass Annotation,
  or ABBA. An ABBA is any four-character sequence which consists of a
  pair of two different characters followed by the reverse of that
  pair, such as xyyx or abba. However, the IP also must not have an
  ABBA within any hypernet sequences, which are contained by square
  brackets.

  For example:

    abba[mnop]qrst supports TLS (abba outside square brackets).

    abcd[bddb]xyyx does not support TLS (bddb is within square
    brackets, even though xyyx is outside square brackets).

    aaaa[qwer]tyui does not support TLS (aaaa is invalid; the interior
    characters must be different).

    ioxxoj[asdfgh]zxcvbn supports TLS (oxxo is outside square
    brackets, even though it's within a larger string).

  How many IPs in your puzzle input support TLS?

  --- Part Two ---

  You would also like to know which IPs support SSL (super-secret
  listening).

  An IP supports SSL if it has an Area-Broadcast Accessor, or ABA,
  anywhere in the supernet sequences (outside any square bracketed
  sections), and a corresponding Byte Allocation Block, or BAB,
  anywhere in the hypernet sequences. An ABA is any three-character
  sequence which consists of the same character twice with a different
  character between them, such as xyx or aba. A corresponding BAB is
  the same characters but in reversed positions: yxy and bab,
  respectively.

  For example:

    aba[bab]xyz supports SSL (aba outside square brackets with
    corresponding bab within square brackets).

    xyx[xyx]xyx does not support SSL (xyx, but no corresponding yxy).

    aaa[kek]eke supports SSL (eke in supernet with corresponding kek
    in hypernet; the aaa sequence is not related, because the interior
    character must be different).

    zazbz[bzb]cdb supports SSL (zaz has no corresponding aza, but zbz
    has a corresponding bzb, even though zaz and zbz overlap).

  How many IPs in your puzzle input support SSL?
 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
//import java.lang.Integer;
//import java.lang.Math;
//import java.lang.String;
//import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class aoc_07 {

    public static boolean __DEBUG__ = false;

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        for (Scanner sc = new Scanner (System.in);
             sc.hasNext();)
            for (String s: sc.next().split(" "))
                als.add (s);
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    private static String[][] parse_input (final String[] ipv7) {
        // split ipv7 to address / net array
        // even: address, odd: net
        ArrayList <String[]> als = new ArrayList <String[]> ();
        for (String s : ipv7) {
            String[] ss = s.split ("[\\[\\]]");
            als.add (ss);
        }
        return als.toArray (new String[als.size()][]);
    }


    final private static Pattern ptls = Pattern.compile
        ("(\\w)(\\w)\\2\\1");

    final private static Pattern paba = Pattern.compile ("(\\w)(\\w)\\1");

    private static boolean find_abba (final String nas) {
        Matcher mt;
        String g1, g2;
        boolean rf;
        for (rf = false, mt = ptls.matcher (nas); mt.find();) {
            g1 = mt.group (1);
            g2 = mt.group (2);
            if (g1.charAt (0) != g2.charAt (0)) {
                rf = true;
                break;
            }
        }
        return rf;
    }


    private static boolean check_tls_support (final String[] addr) {
        boolean ftls, fa, fn;
        //Matcher ma, mn; // address, net
        int i;
        final int AL = addr.length;
        for (fa = fn = ftls = false, i = 0; i < AL; i += 2, fn = false) {
            fa = find_abba (addr[i]);
            fn = i < (AL-1) ? find_abba (addr[i+1]) : false;
            if (fn) {
                ftls = false;
                break;
            }
            if (fa)
                ftls = true;
        }
        return ftls;
    }


    private static int count_tls (final String[][] ip7p) {
        int ntls = 0, i;
        for (String[] addr : ip7p)
            if (check_tls_support (addr))
                ++ntls;
        return ntls;
    }


    private static boolean find_bab (final String[] addr,
                                     final char c0, final char c1) {
        int i, p;
        boolean fb;
        Matcher ma; // address, net
        for (fb = true, i = 1; i < addr.length && fb; i += 2) {
            for (p = 0, ma = paba.matcher (addr[i]);
                 ma.find (p); p = ma.start() + 1) {
                if (ma.group(1).charAt(0) == c1 &&
                    ma.group(2).charAt(0) == c0) {
                    fb = false;
                    break;
                }
            }
        }
        return !fb;
    }


    private static boolean find_aba_bab (final String[] addr) {
        int i, p;
        boolean fa;
        Matcher ma; // address
        for (fa = true, i = 0; fa && i < addr.length; i += 2) {
            for (p = 0, ma = paba.matcher (addr[i]);
                 fa && ma.find (p); p = ma.start() + 1) {
                fa = !find_bab (addr, ma.group(1).charAt(0),
                                ma.group(2).charAt(0));
            }
        }
        return !fa;
    }


    private static boolean check_ssl_support (final String[] addr) {
        boolean fssl;
        int i;
        final int AL = addr.length;
        for (fssl = true, i = 0; fssl && i < AL; i += 2)
            fssl = !find_aba_bab (addr);
        return !fssl;
    }


    private static int count_ssl (final String[][] ip7p) {
        int nssl = 0, i;
        for (String[] addr : ip7p)
            if (check_ssl_support (addr))
                ++nssl;
        return nssl;
    }


    public static void main (String[] args) throws Exception {
        String[] ip7 = read_input(); if (ip7 == null) return;
        String[][] ip7p = parse_input (ip7);
        int tls, ssl;
        tls = count_tls (ip7p);
        ssl = count_ssl (ip7p);
        System.out.printf ("TLS: %d\nSSL: %d\n", tls, ssl);
    }
}

