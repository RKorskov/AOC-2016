// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-13 16:11:57 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  --- Day 4: Security Through Obscurity ---

  Finally, you come across an information kiosk with a list of
  rooms. Of course, the list is encrypted and full of decoy data, but
  the instructions to decode the list are barely hidden nearby. Better
  remove the decoy data first.

  Each room consists of an encrypted name (lowercase letters separated
  by dashes) followed by a dash, a sector ID, and a checksum in square
  brackets.

  A room is real (not a decoy) if the checksum is the five most common
  letters in the encrypted name, in order, with ties broken by
  alphabetization. For example:

    aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common
    letters are a (5), b (3), and then a tie between x, y, and z,
    which are listed alphabetically.

    a-b-c-d-e-f-g-h-987[abcde] is a real room because although the
    letters are all tied (1 of each), the first five are listed
    alphabetically.

    not-a-real-room-404[oarel] is a real room.

    totally-real-room-200[decoy] is not.

  Of the real rooms from the list above, the sum of their sector IDs
  is 1514.

  What is the sum of the sector IDs of the real rooms?

  --- Part Two ---

  With all the decoy data out of the way, it's time to decrypt this
  list and get moving.

  The room names are encrypted by a state-of-the-art shift cipher,
  which is nearly unbreakable without the right software. However, the
  information kiosk designers at Easter Bunny HQ were not expecting to
  deal with a master cryptographer like yourself.

  To decrypt a room name, rotate each letter forward through the
  alphabet a number of times equal to the room's sector ID. A becomes
  B, B becomes C, Z becomes A, and so on. Dashes become spaces.

  For example, the real name for qzmt-zixmtkozy-ivhz-343 is very
  encrypted name.

  What is the sector ID of the room where North Pole objects are
  stored?
 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
import java.lang.Integer;
//import java.lang.Math;
//import java.lang.String;
import java.lang.StringBuilder;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class aoc_04 {

    public static boolean __DEBUG__ = false;


    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        for (Scanner sc = new Scanner (System.in);
             sc.hasNext();)
            for (String s: sc.next().split(" "))
                als.add (s);
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    private static Room[] parse_input (String[] rdss) {
        /*
          not-a-real-room-404[oarel] :
          room name : not-a-real-room
          sector    : 404
          checksum  : oarel
         */
        ArrayList <Room> alr = new ArrayList <Room> ();
        for (String s : rdss) {
            Room r = Room.make_room (s);
            if (r != null)
                alr.add (r);
        }
        if (alr.size() > 0)
            return alr.toArray (new Room[alr.size()]);
        return null;
    }


    private static String unrotn (final String nr, final int sec) {
        // Rot_N decipher
        final char[] abc = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        final int mod = sec % abc.length;
        int i, j, p, q;
        char c;
        boolean fm;
        StringBuilder rs = new StringBuilder();
        if (mod == 0)
            return nr;

        if (__DEBUG__) System.out.printf ("mod %d\n", mod);

        for (i = 0; i < nr.length(); ++i) {
            c = nr.charAt (i);
            if (c == '-') {
                rs.append (' ');
                continue;
            }

            for (p = -1, j = 0; j < abc.length; ++j)
                if (c == abc[j]) {
                    p = j;
                    break;
                }

            if (__DEBUG__) System.out.printf ("%c %d\n", c, p);
            if (p >= 0) {
                p += mod;
                p %= abc.length;
                if (__DEBUG__) System.out.printf ("   \\--> %c %d\n", abc[p], p);
                rs.append (abc[p]);
            }
            else
                System.out.println ("error?");
        }
        return rs.toString();
    }


    public static void main (String[] args) throws Exception {
        long s;
        int i;
        String[] rdss = read_input();
        if (rdss == null) return;
        System.out.printf ("input lines %d\n", rdss.length);
        Room[] rrms = parse_input (rdss);
        if (rrms == null) return;
        System.out.printf ("valid lines %d\n", rrms.length);
        for (i = 0, s = 0; i < rrms.length; s += rrms[i].sect, ++i);
        System.out.printf ("total sectors sum %d\n", s);
        // 2nd part
        for (i = 0; i < rrms.length; ++i)
            System.out.printf ("%s %d\n",
                               unrotn (rrms[i].name, rrms[i].sect),
                               rrms[i].sect);
    }
}


class Room {
    public static boolean __DEBUG__ = false;

    /*
      not-a-real-room-404[oarel] :
      room name : not-a-real-room
      sector    : 404
      checksum  : oarel
    */

    String name;
    int sect;
    String csum;

    Room (String nm, int sec, String cs) {
        name = nm;
        sect = sec;
        csum = cs;
    }


    public static Room make_room (String rds) {
        Pattern rpat = Pattern.compile ("^([a-z[-]]+)-(\\d+)\\[([a-z]{1,5})\\]$");
        Matcher mr = rpat.matcher (rds);
        boolean fm = mr.matches();
        if (!fm)
            return null;
        if (__DEBUG__) System.out.printf ("'%s'\n", rds);
        String nm = mr.group (1);
        int sc = Integer.valueOf (mr.group (2));
        String cs = mr.group (3);
        if (__DEBUG__) System.out.printf ("%s %d %s\n", nm, sc, cs);
        if (check_csum (nm, cs))
            return new Room (nm, sc, cs);
        return null;
    }


    private static void print_fc (int[] fr, char[] cs) {
        for (int i = 0; i < fr.length; ++i)
            if (cs[i] > 0)
                System.out.printf ("%2d : %c : %2d\n", i, cs[i], fr[i]);
    }


    private static void print_fc (Freq[] frs) {
        for (int i = 0; i < frs.length; ++i)
            System.out.printf ("%2d : %c : %2d\n", i, frs[i].ch, frs[i].fr);
    }


    public static boolean check_csum (String rname, String csum) {
        final int nc = rname.length();
        int i, j;
        char c;
        boolean fif;
        ArrayList <Freq> alf = new ArrayList <Freq> ();
        Freq[] frs;
        Freq f;

        // building freq list
        for (i = 0; i < nc; ++i) {
            c = rname.charAt (i);
            if (c == '-')
                continue;
            // increase freq (or add new entry)
            for (fif = true, j = 0; j < alf.size(); ++j) {
                f = alf.get (j);
                if (f.ch == c) {
                    f.inc();
                    fif = false;
                    break;
                }
            }

            // new entry
            if (fif)
                alf.add (new Freq (c));
        }

        //sort!!!
        alf.sort ((f0, f1) -> {
                if (f0.fr > f1.fr)
                    return -1;
                if (f0.fr < f1.fr)
                    return 1;
                if (f0.ch > f1.ch)
                    return 1;
                if (f0.ch < f1.ch)
                    return -1;
                return 0;
            });

        frs = alf.toArray (new Freq [alf.size()]);
        if (__DEBUG__) System.out.println ("-------- collected");
        if (__DEBUG__) print_fc (frs);

        fif = false;
        if (frs.length < csum.length())
            return false;
        else
        for (fif = true, i = 0; i < 5 && i < frs.length; ++i) {
            if (frs[i].ch != csum.charAt (i)) {
                fif = false;
                break;
            }
        }

        if (__DEBUG__) System.out.println (fif);
        return fif;
    }
}


class Freq {
    char ch;
    int fr;

    Freq() {
        ch = 0;
        fr = 0;
    }

    Freq (char c) {
        ch = c;
        fr = 1;
    }

    Freq (char c, int f) {
        ch = c;
        fr = f;
    }

    public void inc() {
        ++fr;
    }
}
