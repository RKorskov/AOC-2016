// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-20 14:54:04 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  http://adventofcode.com/2016/day/10

  --- Day 10: Balance Bots ---

  You come upon a factory in which many robots are zooming around
  handing small microchips to each other.

  Upon closer examination, you notice that each bot only proceeds when
  it has two microchips, and once it does, it gives each one to a
  different bot or puts it in a marked "output" bin. Sometimes, bots
  take microchips from "input" bins, too.

  Inspecting one of the microchips, it seems like they each contain a
  single number; the bots must use some logic to decide what to do
  with each chip. You access the local control computer and download
  the bots' instructions (your puzzle input).

  Some of the instructions specify that a specific-valued microchip
  should be given to a specific bot; the rest of the instructions
  indicate what a given bot should do with its lower-value or
  higher-value chip.

  For example, consider the following instructions:

value 5 goes to bot 2
bot 2 gives low to bot 1 and high to bot 0
value 3 goes to bot 1
bot 1 gives low to output 1 and high to bot 0
bot 0 gives low to output 2 and high to output 0
value 2 goes to bot 2

    Initially, bot 1 starts with a value-3 chip, and bot 2 starts with
    a value-2 chip and a value-5 chip.

    Because bot 2 has two microchips, it gives its lower one (2) to
    bot 1 and its higher one (5) to bot 0.

    Then, bot 1 has two microchips; it puts the value-2 chip in output
    1 and gives the value-3 chip to bot 0.

    Finally, bot 0 has two microchips; it puts the 3 in output 2 and
    the 5 in output 0.

  In the end, output bin 0 contains a value-5 microchip, output bin 1
  contains a value-2 microchip, and output bin 2 contains a value-3
  microchip. In this configuration, bot number 2 is responsible for
  comparing value-5 microchips with value-2 microchips.

  Based on your instructions, what is the number of the bot that is
  responsible for comparing value-61 microchips with value-17
  microchips?

  --- Part Two ---

  What do you get if you multiply together the values of one chip in
  each of outputs 0, 1, and 2?

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
//import java.lang.Integer;
//import java.lang.Math;
//import java.lang.String;
//import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class aoc_10 {

    public static boolean __DEBUG__ = false;

    /**
       CBT_*_DST :
       0  uninitialized
     */
    final private static int CBT_IDX      = 0;
    final private static int CBT_VAL_1    = 1;
    final private static int CBT_VAL_2    = 2;
    final private static int CBT_LOW_DST  = 3;
    final private static int CBT_LOW_IDX  = 4;
    final private static int CBT_HIGH_DST = 5;
    final private static int CBT_HIGH_IDX = 6;
    final private static int CBT_VAL_NULL = -1;
    final private static int CBT_IDX_NULL = -1;
    final private static int CBT_IDX_CONT = -69;
    final private static int CBT_IDX_BRK  = -34;
    final private static int CBT_DST_NULL = -1;
    final private static int CBT_DST_BOT  = 0; // CBT_(HIGH|LOW)_DST
    final private static int CBT_DST_OUT  = 1; // CBT_(HIGH|LOW)_DST

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    private static int update_bot (ArrayList <int[]> ali,
                                   final int bn, final int vl) {
        // updates bot's values "on-hands"
        int i;
        int[] p;
        for (i = 0; i < ali.size(); ++i) {
            p = ali.get (i);
            if (p[CBT_IDX] == bn) {
                if (p[CBT_VAL_1] == CBT_VAL_NULL)
                    p[CBT_VAL_1] = vl;
                else if (p[CBT_VAL_2] == CBT_VAL_NULL)
                    p[CBT_VAL_2] = vl;
                else
                    System.out.printf ("error: bot idx %d  № %d\n", i, bn);
                return i;
            }
        }

        p = new int[] {bn, vl, CBT_VAL_NULL,
                       CBT_DST_NULL, CBT_IDX_NULL,
                       CBT_DST_NULL, CBT_IDX_NULL};
        ali.add (p);

        return ali.size() - 1;
    }


    private static int get_dst_type (final String dst_type) {
        int rc = CBT_DST_NULL;
        switch (dst_type.toLowerCase()) {
        case "output":
            rc = CBT_DST_OUT;
            break;
        case "bot":
            rc = CBT_DST_BOT;
            break;
        default:
            break;
        }
        return rc;
    }


    private static int update_bot (ArrayList <int[]> ali, final int bn,
                                   final String dst_low_type,
                                   final int dst_low_idx,
                                   final String dst_high_type,
                                   final int dst_high_idx) {
        // updates bot's programm
        final int alis = ali.size();
        int i, rc;
        final int dlt, dht;
        int[] cbt;
        dlt = get_dst_type (dst_low_type);
        dht = get_dst_type (dst_high_type);
        for (rc = -1, i = 0; i < alis; ++i) {
            cbt = ali.get (i);
            if (cbt[CBT_IDX] == bn) {
                cbt[CBT_LOW_DST] = dlt;
                cbt[CBT_HIGH_DST] = dht;
                cbt[CBT_LOW_IDX] = dst_low_idx;
                cbt[CBT_HIGH_IDX] = dst_high_idx;
                rc = i;
                break;
            }
        }

        if (rc == -1) {
            cbt = new int[] {bn, CBT_VAL_NULL, CBT_VAL_NULL,
                             dlt, dst_low_idx, dht, dst_high_idx};
            ali.add (cbt);
            rc = ali.size() - 1;
        }

        return rc;
    }


    private static int update_bot (int[][] cbts,
                                   final int idx, final int val) {
        // bot idx receives value val
        int i, rc, vl, vh;
        int[] cbt;
        for (rc = CBT_IDX_NULL, i = 0; i < cbts.length; ++i)
            if (cbts[i][CBT_IDX] == idx) {
                cbt = cbts[i];
                vl = min (cbt[CBT_VAL_1], cbt[CBT_VAL_2]);
                vh = max (cbt[CBT_VAL_1], cbt[CBT_VAL_2]);
                if (vl == CBT_VAL_NULL)
                    vl = val;
                else if (vh == CBT_VAL_NULL)
                    vh = val;
                else
                    System.out.printf
                        ("error bot idx %d  № %d  val %d\n", i, idx, val);
                cbt[CBT_VAL_1] = min (vl, vh);
                cbt[CBT_VAL_2] = max (vl, vh);
                rc = i;
                break;
            }
        return rc;
    }


    private static int update_output (int[][] cbts,
                                      final int idx, final int val) {
        // output bin idx received value val
        System.out.printf ("out bin № %d  val %d\n", idx, val);
        return idx;
    }


    // "value 17 goes to bot 4"
    final private static Pattern psv = Pattern.compile
        ("^value (\\d+) goes to bot (\\d+)$");

    // bot 1 gives low to output 1 and high to bot 0
    // bot 0 gives low to output 2 and high to output 0
    final private static Pattern psr = Pattern.compile
        ("bot (\\d+) gives low to (output|bot) (\\d+) and high to (output|bot) (\\d+)");

    // {bot_id, val0, val1, low_id, high_id}
    private static int[][] start_vals (final String[] istr) {
        ArrayList <int[]> ali = new ArrayList <int[]> ();
        Matcher m;
        int b, v, p;
        int[] cb;
        String dlt, dht; // dest low type, dest high type
        int dli, dhi; // dest low idx, dest high idx
        for (String s : istr) {
            //System.out.println (s);
            m = psv.matcher (s); // startin values
            if (m.find()) {
                v = Integer.parseInt (m.group (1));
                b = Integer.parseInt (m.group (2));
                p = update_bot (ali, b, v);
                //System.out.printf ("  %d %d %d\n", p, b, v);
                continue;
            }

            m = psr.matcher (s);
            if (m.find()) { // rules
                b = Integer.parseInt (m.group (1));
                dlt = m.group (2);
                dli = Integer.parseInt (m.group (3));
                dht = m.group (4);
                dhi = Integer.parseInt (m.group (5));
                p = update_bot (ali, b, dlt, dli, dht, dhi);
                //System.out.printf ("  %d %d %d\n", p, b, v);
                continue;
            }
        }

        if (ali.size() > 0)
            return ali.toArray (new int[ali.size()][]);
        return null;
    }


    private static int max (final int a, final int b) { return a > b ? a : b; }

    private static int min (final int a, final int b) { return a < b ? a : b; }


    private static int give_away (int[][] cbts, int idx)
    {
        int i, tvl, tvh, vl, vh, t, rc, fml, fmh;
        int[] cbt;

        fml = fmh = CBT_IDX_NULL;
        rc = CBT_IDX_NULL;
        cbt = cbts[idx];
        vl = cbt[CBT_VAL_1];
        vh = cbt[CBT_VAL_2];
        t = max (vl, vh);
        vl = min (vl, vh);
        vh = t;

        System.out.printf ("give away : vl %d  vh %d\n", vl, vh);

        if (vl != CBT_VAL_NULL)
            switch (cbt[CBT_LOW_DST]) {
            case CBT_DST_BOT:
                fml = update_bot (cbts, cbt[CBT_LOW_IDX], vl);
                System.out.printf ("bot idx %d  № %d low to bot %d  val %d\n",
                                   idx, cbt[CBT_IDX], cbt[CBT_LOW_IDX], vl);
                vl = CBT_VAL_NULL;
                break;
            case CBT_DST_OUT:
                update_output (cbts, cbt[CBT_LOW_IDX], vl);
                vl = CBT_VAL_NULL;
                break;
            default:
                System.out.printf
                    ("error bot idx %d  № %d  dst %d\n",
                     idx, cbt[CBT_IDX], cbt[CBT_LOW_DST]);
        }
        cbt[CBT_VAL_1] = vl;

        if (vh != CBT_VAL_NULL)
            switch (cbt[CBT_HIGH_DST]) {
            case CBT_DST_BOT:
                fmh = update_bot (cbts, cbt[CBT_HIGH_IDX], vh);
                System.out.printf ("bot idx %d  № %d high to bot %d  val %d\n",
                                   idx, cbt[CBT_IDX], cbt[CBT_HIGH_IDX], vh);
                vh = CBT_VAL_NULL;
                break;
            case CBT_DST_OUT:
                update_output (cbts, cbt[CBT_HIGH_IDX], vh);
                vh = CBT_VAL_NULL;
                break;
            default:
                System.out.printf
                    ("error bot idx %d  № %d  dst %d\n",
                     idx, cbt[CBT_IDX], cbt[CBT_HIGH_DST]);
            }
        cbt[CBT_VAL_2] = vh;

        rc = max (fmh, fml);
        return rc;
    }


    private static int process_bots (int[][] cbts,
                                     final int val0, final int val1) {
        // returns idx of bot, who is comparing val0 agains val1
        int i, vl, vh, t, rc, fm;
        final int tvl, tvh;
        int[] cbt;
        tvl = min (val0, val1);
        tvh = max (val0, val1);
        for (fm = CBT_IDX_BRK, rc = CBT_IDX_BRK, i = 0;
             i < cbts.length; ++i) {
            cbt = cbts[i];
            vl = cbt[CBT_VAL_1];
            vh = cbt[CBT_VAL_2];
            t = max (vl, vh);
            vl = min (vl, vh);
            vh = t;

            if (vl == tvl && vh == tvh) {
                //return cbt[CBT_IDX];
                rc = cbt[CBT_IDX];
                break;
            }

            if (vl != CBT_VAL_NULL && vh != CBT_VAL_NULL) {
                t = give_away (cbts, i);
                if (t != CBT_IDX_NULL)
                    rc = CBT_IDX_CONT;
                System.out.printf ("give away idx %d  rc %d %d\n", i, t, rc);
            }
        }

        return rc;
    }


    private static void cbts_print (int[][] cbts) {
        int i, j;
        int[] cbt;
        for (i = 0; i < cbts.length; ++i) {
            cbt = cbts[i];
            for (j = 0; j < 7; ++j)
                System.out.printf ("% 3d ", cbt[j]);
            System.out.println();
        }
    }


    public static void main (String[] args) throws Exception {
        String[] cmds = read_input(); if (cmds == null) return;
        //CBot[] cbs = parse_input (cmds); if (cbs == null) return;
        int[][] cbts = start_vals (cmds);
        if (cbts == null) return;
        /*
        for (int i = 0; i < cbts.length; ++i)
            System.out.printf ("%3d %3d %3d\n",
                               cbts[i][0], cbts[i][1], cbts[i][2]);
        */
        cbts_print (cbts);
        int bn, i;
        for (i = 0, bn = CBT_IDX_CONT; bn == CBT_IDX_CONT; ++i) {
            bn = process_bots (cbts, 0, 0);
            System.out.printf ("---------------- loop %d  rc %d\n", i, bn);
            cbts_print (cbts);
        }
        System.out.println (bn);
    }
}
