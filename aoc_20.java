// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-27 17:23:48 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  http://adventofcode.com/2016/day/20

  --- Day 20: Firewall Rules ---

You'd like to set up a small hidden computer here so you can use it to get back into the network later. However, the corporate firewall only allows communication with certain external IP addresses.

You've retrieved the list of blocked IPs from the firewall, but the list seems to be messy and poorly maintained, and it's not clear which IPs are allowed. Also, rather than being written in dot-decimal notation, they are written as plain 32-bit integers, which can have any value from 0 through 4294967295, inclusive.

For example, suppose only the values 0 through 9 were valid, and that you retrieved the following blacklist:

5-8
0-2
4-7

The blacklist specifies ranges of IPs (inclusive of both the start and end value) that are not allowed. Then, the only IPs that this firewall allows are 3 and 9, since those are the only numbers not in any range.

Given the list of blocked IPs you retrieved from the firewall (your puzzle input), what is the lowest-valued IP that is not blocked?

How many IPs are allowed by the blacklist?

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
//import java.lang.Integer;
import java.lang.Long;
//import java.lang.Math;
import java.lang.String;
//import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
//import java.util.regex.Pattern;
//import java.util.regex.Matcher;

public class aoc_20 {

    public final static boolean __DEBUG__ = false;
    //public final static boolean __DEBUG__ = true;

    private static int max (final int a, final int b) { return a > b ? a : b; }

    private static int min (final int a, final int b) { return a < b ? a : b; }

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    private static long[][] parse_input (String[] sdata) {
        ArrayList <long[]> ali = new ArrayList <long[]> ();
        String[] du;
        long[] idu;
        for (String s: sdata) {
            du = s.split ("-");
            idu = new long[2];
            idu[0] = Long.parseLong (du[0]);
            idu[1] = Long.parseLong (du[1]);
            ali.add (idu);
        }
        return ali.toArray (new long [ali.size()][]);
    }


    private static long scan_range (final long[][] rngs) {
        return scan_range (rngs, 0);
    }


    private static long scan_range (final long[][] rngs, final long frm) {
        int i, j;
        boolean rf;
        for (i = (int)frm; i >= 0; ++i) {
            for (rf = true, j = 0; j < rngs.length; ++j) {
                if (i <= rngs[j][1] && i >= rngs[j][0]) {
                    rf = false;
                    break;
                }
            }
            if (rf)
                return i;
        }
        return -7;
    }


    private static long check_ranges (final long[][] rngs) {
        long p, q;
        for (q = 0, p = 0; p <= 0xFFFFFFFF; ++p) {
            p = scan_range (rngs, p);
            //System.out.println (p);
            if (p >= 0)
                ++q;
        }
        return q;
    }


    private static long[] overlap (long[][] rng, final int i, final int j) {
        long a0, a1, b0, b1;
        a0 = rng[i][0];
        b0 = rng[j][0];
        if (a0 <= b0) {
            a1 = rng[i][1];
            b1 = rng[j][1];
        }
        else {
            a0 = rng[j][0];
            b0 = rng[i][0];
            a1 = rng[j][1];
            b1 = rng[i][1];
        }
        if (a0 < 0 || a1 < 0 || b0 < 0 || b1 < 0) return null;
        // (a0,a1) <= (b0,b1)
        //a0 -= a0;
        //a1 -= a0;
        //b0 -= a0;
        //b1 -= a0;
        // // (0,a1') <= (b0',b1')
        if ((b0 - a1) > 1)
            return null;
        if (a1 <= b1)
            a1 = b1;
        return new long[] {a0, a1};
    }


    private static long[][] squeeze (final long[][] rngs) {
        ArrayList <long[]> ali = new ArrayList <long[]> ();
        int i;
        for (i = 0; i < rngs.length; ++i)
            if (rngs[i][0] >= 0 && rngs[i][1] >= 0)
                ali.add (rngs[i]);
        ali.sort ((u0, u1) -> {
                if (u0[0] > u1[0])
                    return 1;
                if (u0[0] < u1[0])
                    return -1;
                return 0;
            });
        return ali.toArray (new long [ali.size()][]);
    }


    private static long[][] merge_ranges (final long[][] rngs) {
        long[][] rng;
        long[] uij;
        int i, j;
        boolean rf;
        for (rf = true; rf;)
            for (rf = false, i = 0; i < rngs.length; ++i) {
                for (j = i + 1; j < rngs.length; ++j) {
                    uij = overlap (rngs, i, j);
                    if (uij != null) {
                        rngs[i][0] = uij[0];
                        rngs[i][1] = uij[1];
                        rngs[j][0] = -1;
                        rngs[j][1] = -1;
                        rf = true;
                    }
                }
            }
        rng = squeeze (rngs);
        return rng;
    }


    private static void print (final long[][] rngs) {
        int i;
        for (i = 0; i < rngs.length; ++i)
            System.out.printf ("%d-%d\n", rngs[i][0], rngs[i][1]);
    }


    public static void main (String[] args) throws Exception {
        //final String salt;
        //salt = args.length > 0 ? args[0] : "ahsbgdzn";
        String[] sdata = read_input(); if (sdata == null) return;
        long[][] rngs = parse_input (sdata); if (rngs == null) return;
        //System.out.printf ("total %d\n", check_ranges (rngs));
        //print (rngs);
        long[][] mrng = merge_ranges (rngs);
        print (mrng);
        long c;
        int i;
        for (i = 0, c = 0; i < (mrng.length - 1); ++i) {
            c += mrng[i+1][0] - mrng[i][1] - 1;
        }
        System.out.println (c);
    }
}

