// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2017-01-13 14:08:08 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  602214085774
  http://adventofcode.com/2016/day/22
  korskov.r.v@gmail.com

  --- Day 22: Grid Computing ---

  You gain access to a massive storage cluster arranged in a grid;
  each storage node is only connected to the four nodes directly
  adjacent to it (three if the node is on an edge, two if it's in a
  corner).

  You can directly access data only on node /dev/grid/node-x0-y0, but
  you can perform some limited actions on the other nodes:

    You can get the disk usage of all nodes (via df). The result of
    doing this is in your puzzle input.

    You can instruct a node to move (not copy) all of its data to an
    adjacent node (if the destination node has enough space to receive
    the data). The sending node is left empty after this operation.

  Nodes are named by their position: the node named node-x10-y10 is
  adjacent to nodes node-x9-y10, node-x11-y10, node-x10-y9, and
  node-x10-y11.

  Before you begin, you need to understand the arrangement of data on
  these nodes. Even though you can only move data between directly
  connected nodes, you're going to need to rearrange a lot of the data
  to get access to the data you need. Therefore, you need to work out
  how you might be able to shift data around.

  To do this, you'd like to count the number of viable pairs of
  nodes. A viable pair is any two nodes (A,B), regardless of whether
  they are directly connected, such that:

    Node A is not empty (its Used is not zero).

    Nodes A and B are not the same node.

    The data on node A (its Used) would fit on node B (its Avail).

  How many viable pairs of nodes are there?

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
import java.lang.Integer;
//import java.lang.Long;
//import java.lang.Math;
import java.lang.String;
//import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class aoc_22 {

    //public final static boolean __DEBUG__ = false;
    public final static boolean __DEBUG__ = true;
    //public final static boolean __DEBUG2__ = false;
    //public final static boolean __DEBUG2__ = true;

    private static int max (final int a, final int b) { return a > b ? a : b; }

    private static int min (final int a, final int b) { return a < b ? a : b; }

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    //  Filesystem              Size  Used  Avail  Use%
    //  /dev/grid/node-x0-y0     85T   67T    18T   78%
    final static Pattern gpat = Pattern.compile
        ("/dev/grid/node-x(\\d+)-y(\\d+)\\s+(\\d+)T\\s+(\\d+)T\\s+(\\d+)T\\s+(\\d+)%");

    private static Grid parse_input (final String[] sdata) {
        ArrayList <int[]> ali = new ArrayList <int[]> ();
        Matcher sm;
        int x, y, sz, u, mx, my, a;
        Grid g;
        mx = my = -1;
        for (String s : sdata) {
            sm = gpat.matcher (s);
            if (sm.matches()) {
                x = (int) Integer.parseInt (sm.group (1));
                y = (int) Integer.parseInt (sm.group (2));
                sz = (int) Integer.parseInt (sm.group (3));
                u = (int) Integer.parseInt (sm.group (4));
                a = (int) Integer.parseInt (sm.group (5));
                if (__DEBUG__)
                    System.out.printf
                        ("x %d  y %d  s %d  u %d\n", x, y, sz, u);
                if (sz != (u+a))
                    System.out.printf
                        ("error: %d %d  %d %d %d\n", x, y, sz, u, a);
                ali.add (new int[] {x, y, sz, u});
                if (x > mx)
                    mx = x;
                if (y > my)
                    my = y;
            }
        }

        if (__DEBUG__) System.out.printf ("mx %d  my %d\n", mx, my);
        if (mx <= 0 || my <= 0)
            return null;
        g = new Grid (mx+1, my+1);
        if (__DEBUG__)
            System.out.printf
                ("g  %d : %d\n", g.ngrid.length, g.ngrid[0].length);
        for (int[] p : ali) {
            if (__DEBUG__)
                System.out.printf
                    ("%2d  %2d  %4d  %4d\n",
                     p[0], p[1], p[2], p[3]);
            g.update_node (p);
        }
        return g;
    }


    private static int vpairs (Grid g, final int u, final int x, final int y) {
        int vps, i, j;
        for (vps = i = 0; i < g.get_grid_size_x(); ++i)
            for (j = 0; j < g.get_grid_size_y(); ++j) {
                if (i != x && j != y)
                    if (u <= g.get_avail (i, j)) {
                        ++vps;
                        if (__DEBUG__)
                            System.out.printf
                                ("= %02d%02d%02d%02d\n", i, j, x, y);
                    }
            }
        return vps;
    }


    private static int vpairsall (Grid g) {
        int vps, i, j, u;
        for (vps = i = 0; i < g.get_grid_size_x(); ++i)
            for (j = 0; j < g.get_grid_size_y(); ++j) {
                u = g.get_used (i, j);
                if (u > 0)
                    vps += vpairs (g, u, i, j);
            }
        return vps;
    }


    public static void main (String[] args) throws Exception {
        final String ssrc, dsrc;
        //ssrc = args.length > 0 ? args[0] : "abcdefgh";
        //dsrc = args.length > 1 ? args[1] : "fbgdceah";
        String[] sdata = read_input(); if (sdata == null) return;
        Grid grid = parse_input (sdata); if (grid == null) return;
        //grid.print();
        System.out.println (vpairsall (grid));
    }
}


class Node {
    int size, used;

    Node() {
        size = 0;
        used = 0;
    }

    Node (final int s, final int u) {
        size = s;
        used = u;
    }

    public void set_size (final int sz) {
        if (sz >= 0)
            size = sz;
    }

    public void set_used (final int us) {
        if (us >= 0 && us <= size)
            used = us;
    }

    public int get_avail () {
        return size - used;
    }

    public boolean is_fit (Node nd) {
        /**
           checks, is given node nd fits to current node
         */
        return ((size - used) >= nd.used) ? true : false;
    }

    public boolean is_fit_to (Node nd) {
        /**
           checks, is current node fits to given node nd
         */
        return (nd.get_avail() >= used) ? true : false;
    }

    public boolean is_empty () {
        return used <= 0 ? true : false;
    }

    public boolean is_full () {
        return used >= size ? true : false;
    }

    public void print () {
        System.out.printf ("%4d %4d\n", size, used);
    }
}


class Grid {
    Node[][] ngrid; // x:y

    Grid() {
        this (32, 29);
    }

    Grid (final int szx, final int szy) {
        ngrid = new Node[szx][szy];
    }


    public void update_node
        (final int x, final int y, final int sz, final int us) {
        // this.update_node_size (x, y, sz);
        // this.update_node_usage (x, y, us);
        if (x >= 0 && y >= 0 && x < ngrid.length && y < ngrid[y].length &&
            us >= 0 && sz >= 0 && us <= sz) {
            if (ngrid[x][y] == null)
                ngrid[x][y] = new Node (sz, us);
            else {
                ngrid[x][y].size = sz;
                ngrid[x][y].used = us;
            }
        }
    }


    public void update_node
        (final int[] p) {
        // p[] : x, y, size, use
        // this.update_node_size (x, y, sz);
        // this.update_node_use (x, y, us);
        if (p[0] >= 0 && p[1] >= 0 &&
            p[0] < ngrid.length && p[1] < ngrid[0].length &&
            p[2] >= 0 && p[3] >= 0 && p[2] >= p[3]) {
            Node nd = ngrid[p[0]][p[1]];
            if (nd != null) {
                nd.size = p[2];
                nd.used = p[3];
            }
            else
                ngrid[p[0]][p[1]] = new Node (p[2], p[3]);
        }
    }


    public void update_node_size
        (final int x, final int y, final int sz) {
        if (x >= 0 && y >= 0 && x < ngrid.length && y < ngrid[y].length &&
            sz >= 0 && ngrid[x][y].used <= sz)
            ngrid[x][y].size = sz;
    }


    public void update_node_use
        (final int x, final int y, final int us) {
        if (x >= 0 && y >= 0 && x < ngrid.length && y < ngrid[y].length &&
            us >= 0 && us <= ngrid[x][y].size)
            ngrid[x][y].used = us;
    }


    public int get_grid_size_x () {
        return ngrid.length;
    }

    public int get_grid_size_y () {
        return ngrid[0].length;
    }


    public int get_used (final int x, final int y) {
        return ngrid[x][y].used;
    }

    public int get_size (final int x, final int y) {
        return ngrid[x][y].size;
    }

    public int get_avail (final int x, final int y) {
        return ngrid[x][y].size - ngrid[x][y].used;
    }


    public void print () {
        int x, y;
        for (x = 0; x < ngrid.length; ++x) {
            for (y = 0; y < ngrid[0].length; ++y) {
                if (ngrid[x][y] != null)
                    //ngrid[x][y].print();
                    System.out.printf ("%2d %2d  %4d %4d\n",
                                       x, y,
                                       ngrid[x][y].size, ngrid[x][y].used);
                else
                    System.out.printf ("%2d %2d  null\n", x, y);
            }
        }
    }

}
