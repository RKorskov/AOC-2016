/*
  -*- coding: utf-8; indent-tabs-mode: nil; -*-
  -*- eval: (set-language-environment Russian) -*-
  Time-stamp: <2016-12-16 10:07:34 korskov>
 */

/*
  --- Day 8: Two-Factor Authentication ---

  You come across a door implementing what you can only assume is an
  implementation of two-factor authentication after a long game of
  requirements telephone.

  To get past the door, you first swipe a keycard (no problem; there
  was one on a nearby desk). Then, it displays a code on a little
  screen, and you type that code on a keypad. Then, presumably, the
  door unlocks.

  Unfortunately, the screen has been smashed. After a few minutes,
  you've taken everything apart and figured out how it works. Now you
  just have to work out what the screen would have displayed.

  The magnetic strip on the card you swiped encodes a series of
  instructions for the screen; these instructions are your puzzle
  input. The screen is 50 pixels wide and 6 pixels tall, all of which
  start off, and is capable of three somewhat peculiar operations:

    rect AxB turns on all of the pixels in a rectangle at the top-left
    of the screen which is A wide and B tall.

    rotate row y=A by B shifts all of the pixels in row A (0 is the
    top row) right by B pixels. Pixels that would fall off the right
    end appear at the left end of the row.

    rotate column x=A by B shifts all of the pixels in column A (0 is
    the left column) down by B pixels. Pixels that would fall off the
    bottom appear at the top of the column.

For example, here is a simple sequence on a smaller screen:

    rect 3x2 creates a small rectangle in the top-left corner:

    ###....
    ###....
    .......

    rotate column x=1 by 1 rotates the second column down by one pixel:

    #.#....
    ###....
    .#.....

    rotate row y=0 by 4 rotates the top row right by four pixels:

    ....#.#
    ###....
    .#.....

    rotate column x=1 by 1 again rotates the second column down by one
    pixel, causing the bottom pixel to wrap back to the top:

    .#..#.#
    #.#....
    .#.....

  As you can see, this display technology is extremely powerful, and
  will soon dominate the tiny-code-displaying-screen market. That's
  what the advertisement on the back of the display tries to convince
  you, anyway.

  There seems to be an intermediate check of the voltage used by the
  display: after you swipe your card, if the screen did work, how many
  pixels should be lit?

  --- Part Two ---

  You notice that the screen is only capable of displaying capital
  letters; in the font it uses, each letter is 5 pixels wide and 6
  tall.

  After you swipe your card, what code is the screen trying to display?
 */

#define __DEBUG__   1
//#define __DEBUG_1__ 1
//#define __DEBUG_2__ 2
//#define __DEBUG_3__ 3
//#define __DEBUG_4__ 4

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#define SILEN 1024

// 64 bits
typedef long long unsigned int ULL;
#define ULL_BIT_LEN 64

typedef struct tcdscreen {
  ULL *scr; // = {0,0,0,0,0,0}; // 50x6
  int wdt, hgt;
  ULL xmask;
} TCDS;


int rect (TCDS *scr, int rwdt, int rhgt) {
  int i;
  ULL msk, *q;
  if (rwdt < 0 || rwdt >= scr->wdt || rhgt < 0 || rhgt >= scr->hgt)
    return -1;
  // todo!
  msk = 1;
  msk <<= rwdt;
  --msk;
  for (q = scr->scr, i = 0; i < rhgt; ++i, ++q)
    (*q) |= msk;
  return rwdt * rhgt;
}


void ull2binr (char *buf, ULL val) {
  int i;
  ULL p;
  for (p = 1, i = 0; i < ULL_BIT_LEN; ++i, p <<= 1) {
    buf[i] = (val & p) ? '1' : '0';
  }
}


ULL ror_1 (ULL val, int len, int rep) {
  ULL nr, nl, rm;
  int cl;
  int t;
  #ifdef __DEBUG__
  printf ("crot %016Lx %i %i\n", val, len, rep);
  #endif
  cl = rep % len;
  if (cl == 0) return 0;
  #ifdef __DEBUG__
  {
    char buf[128];
    bzero (buf, 128);
    printf ("  val %016Lx\t cl %i\t rm %016Lx\n", val, cl, rm);
    //printf ("  %s\n", buf);
  }
  #endif
  nr = val << cl;
  rm = 1;
  rm <<= cl;
  --rm;
  rm = ~rm;
  t = len - cl - 1;
  rm <<= t;
  nl = (val & rm) >> t;
  #ifdef __DEBUG__
  {
    char buf[128];
    bzero (buf, 128);
    printf ("  nl %016Lx\t nr %016Lx\t %i\n", nl, nr, t);
    //printf ("  %s\n", buf);
  }
  #endif
  return nl | nr;
}


ULL ror (ULL val, int len, int rep) {
  ULL nr, nl, mr, mn, n;
  int cl, i;
  #ifdef __DEBUG__
  printf ("crot %016Lx %i %i\n", val, len, rep);
  #endif
  cl = rep % len;
  if (cl == 0) return 0;
  mr = 1;
  mr <<= len;
  for (n = val, i = 0; i < cl; ++i) {
    n <<= 1;
    n |= 1;
    if (!(n & mr))
      n ^= 1;
  }
  --mr;
  n &= mr;
  #ifdef __DEBUG__
  printf ("\t%i %016Lx %016Lx\n", cl, n, mr);
  #endif
  return n;
}


ULL rorn (ULL val, int len, int rep) {
  ULL nr, nl, rm, t;
  int cl;
  #ifdef __DEBUG__
  printf ("crot %016Lx %i %i\n", val, len, rep);
  #endif
  cl = rep % len;
  if (cl == 0) return 0;

  nr = nl = val;
  rm = 1;
  rm <<= (len - cl);
  --rm;
  nr &= rm;
  nr <<= cl;

  t = ~rm;
  nl &= t;
  nl >>= len - cl - 1;

  #ifdef __DEBUG__
  {
    char buf[128];
    bzero (buf, 128);
    printf ("  val %016Lx\t cl %i\t rm (R)%016Lx (L)%016Lx\n", val, cl, rm, t);
    //printf ("  %s\n", buf);
  }
  #endif

  #ifdef __DEBUG__
  {
    char buf[128];
    bzero (buf, 128);
    printf ("  nl %016Lx\t nr %016Lx\t\n", nl, nr);
    //printf ("  %s\n", buf);
  }
  #endif
  return nl | nr;
}


int crotx (TCDS *scr,  int scol, int rep) {
  // cyclic rotate vertical
  ULL col, p, q, *w;
  int i;
  if (scol < 0 || scol >= scr->wdt) return -1;

  for (q = 1, q <<= scol, p = 1, w = scr->scr, i = 0, col = 0;
       i < scr->hgt;
       ++i, ++w, p <<= 1)
    if (((*w) & q) != 0)
      col |= p;

  col = ror (col, scr->hgt, rep);
  #ifdef __DEBUG_3__
  printf ("crotx %016Lx\t %016Lx\t %016Lx\n", col, q, ~q);
  #endif

  for (q = ~q, w = scr->scr, i = 0, p = 1;
       i < scr->hgt;
       ++i, p <<= 1, ++w) {
    (*w) &= q; // reset shifted bit
    if ((col & p) != 0)
      (*w) |= ~q;
  }
  return 0;
}


int croty (TCDS *scr, int row, int rep) {
  // cyclic rotate horisontal
  #ifdef __DEBUG_2__
  printf ("croty %i %i\n", row, rep);
  #endif
  if (row < 0 || row >= scr->hgt) return -1;
  scr->scr[row] = ror (scr->scr[row], scr->wdt, rep);
  return scr->scr[row];
}


TCDS * init_screen (TCDS *scr, int wdt, int hgt) {
  scr->scr = NULL;
  if (wdt < 1 || wdt > ULL_BIT_LEN)
    return NULL;
  scr->wdt = wdt;
  scr->hgt = hgt;
  scr->xmask = (1 << wdt) - 1;
  scr->scr = (ULL *) calloc (hgt, sizeof (ULL));
  if (scr->scr == NULL)
    return NULL;
  bzero (scr->scr, hgt * sizeof (ULL));
  return scr;
}


void print_screen (TCDS *scr) {
  int i, j;
  ULL *q, p;
  char c;
  if (scr == NULL || scr->scr == NULL || scr->wdt < 0 || scr->hgt < 0) return;

  for (q = scr->scr, i = 0; i < scr->hgt; ++i, ++q) {
    for (p = 1, j = 0; j < scr->wdt; ++j, p <<= 1) {
      c = (((*q) & p) != 0) ? '#' : '.';
      putchar (c);
    }
    putchar ('\n');
  }

  #ifdef __DEBUG_4__
  for (q = scr->scr, i = 0; i < scr->hgt; ++i, ++q)
    for (p = 1, j = 0; j < scr->wdt; ++j, p <<= 1)
      if (((*q) & p) != 0)
        printf ("0x%016Lx\t 0x%016Lx\t %i %i\n", *q, p, i, j);
  #endif

  for (q = scr->scr, i = 0; i < scr->hgt; ++i, ++q)
    printf ("0x%016Lx\n", *q);
  return;
}


int bits_count (TCDS *scr) {
  int i, j, n;
  ULL p, *w;
  for (w = scr->scr, n = j = 0; j < scr->hgt; ++j, ++w)
    for (p = 1, i = 0; i < scr->wdt; ++i, p <<= 1)
      if (((*w) & p) != 0)
        ++n;
  return n;
}


int main (int argc, char **argv) {
  TCDS scr;
  char si[SILEN+1];

  init_screen (&scr, 50, 6);

  for(*si = 0; feof (stdin) == 0; *si = 0) {
    fgets (si, SILEN, stdin);
    #ifdef __DEBUG__
    printf ("readed: %s\n", si);
    #endif

    if (strncmp (si, "rect ", 5) == 0) {
      int x, y;
      sscanf (si + 5, "%ix%i", &x, &y);
      #ifdef __DEBUG_1__
      printf ("rect %i %i\n", x, y);
      #else
      rect (&scr, x, y);
      #endif
      continue;
    }

    if (strncmp (si, "rotate row y=", 13) == 0) {
      int y, rep;
      sscanf (si + 13, "%i by %i", &y, &rep);
      #ifdef __DEBUG_1__
      printf ("yrot %i %i\n", y, rep);
      #else
      croty (&scr, y, rep);
      #endif
      continue;
    }

    if (strncmp (si, "rotate column x=", 16) == 0) {
      int x, rep;
      sscanf (si + 16, "%i by %i", &x, &rep);
      #ifdef __DEBUG_1__
      printf ("xrot %i %i\n", x, rep);
      #else
      crotx (&scr, x, rep);
      #endif
      continue;
    }
  }

  print_screen (&scr);
  printf ("on-bits %i\n", bits_count (&scr));

  if (scr.scr != NULL)
    free (scr.scr);
  return 0;
}

/*
 vim: shiftwidth=2 smarttab expandtab autoindent softtabstop=2 foldminlines=7 foldmethod=indent foldcolumn=2
*/
