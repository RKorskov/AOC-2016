// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-13 11:16:04 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  Now that you can think clearly, you move deeper into the labyrinth
  of hallways and office furniture that makes up this part of Easter
  Bunny HQ. This must be a graphic design department; the walls are
  covered in specifications for triangles.

  Or are they?

  The design document gives the side lengths of each triangle it
  describes, but... 5 10 25? Some of these aren't triangles. You can't
  help but mark the impossible ones.

  In a valid triangle, the sum of any two sides must be larger than
  the remaining side. For example, the "triangle" given above is
  impossible, because 5 + 10 is not larger than 25.

  In your puzzle input, how many of the listed triangles are possible?

  --------------------------------

  Now that you've helpfully marked up their design documents, it
  occurs to you that triangles are specified in groups of three
  vertically. Each set of three numbers in a column specifies a
  triangle. Rows are unrelated.

  For example, given the following specification, numbers with the
  same hundreds digit would be part of the same triangle:

  101 301 501
  102 302 502
  103 303 503
  201 401 601
  202 402 602
  203 403 603

  In your puzzle input, and instead reading by columns, how many of
  the listed triangles are possible?
 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
//import java.lang.Integer;
//import java.lang.Math;
//import java.lang.String;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
//import java.util.regex.Pattern;
//import java.util.regex.Matcher;

public class aoc_03 {

    public static boolean __DEBUG__ = false;


    private static int[][] read_input () {
        int[][] strs;
        int i;
        ArrayList <int[]> als = new ArrayList <int[]> ();
        for (Scanner sc = new Scanner (System.in);
             sc.hasNextInt();) {
            int trn[] = new int [3];
            for (i = 0; i < 3 && sc.hasNextInt(); ++i) {
                trn[i] = sc.nextInt();
            }
            als.add (trn);
        }
        strs = new int [als.size()][3];
        return als.toArray (strs);
    }


    private static int count_valid (int[][] trn) {
        int j, a, b, c, cnt;
        final int NFIG, NEDG;
        NFIG = trn.length;
        NEDG = trn[0].length;
        for (cnt = j = 0; j < NFIG; ++j) {
            a = trn[j][0];
            b = trn[j][1];
            c = trn[j][2];
            if ((a+b) > c && (a+c) > b && (b+c) > a)
                ++cnt;
        }
        return cnt;
    }


    private static int count_valid_v (int[][] trn) {
        int i, j, a, b, c, cnt;
        final int NCOL, NROW;
        NROW = trn.length;
        NCOL = trn[0].length;
        for (cnt = i = 0; i < NCOL; ++i)
            for (j = 0; j < (NROW - 2); j += 3) {
                a = trn[j][i];
                b = trn[j+1][i];
                c = trn[j+2][i];
                if ((a+b) > c && (a+c) > b && (b+c) > a)
                    ++cnt;
            }
        return cnt;
    }


    public static void main (String[] args) throws Exception {
        int n;
        int[][] trng = read_input();
        n = count_valid (trng);
        System.out.println (n);
        n = count_valid_v (trng);
        System.out.println (n);
    }
}

