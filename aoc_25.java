// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-26 17:43:23 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  http://adventofcode.com/2016/day/25
  korskov.r.v@gmail.com

  --- Day 25: Clock Signal ---

  You open the door and find yourself on the roof. The city sprawls
  away from you for miles and miles.

  There's not much time now - it's already Christmas, but you're
  nowhere near the North Pole, much too far to deliver these stars to
  the sleigh in time.

  However, maybe the huge antenna up here can offer a solution. After
  all, the sleigh doesn't need the stars, exactly; it needs the timing
  data they provide, and you happen to have a massive signal generator
  right here.

  You connect the stars you have to your prototype computer, connect
  that to the antenna, and begin the transmission.

    Nothing happens.

  You call the service number printed on the side of the antenna and
  quickly explain the situation. "I'm not sure what kind of equipment
  you have connected over there," he says, "but you need a clock
  signal." You try to explain that this is a signal for a clock.

  "No, no, a clock signal - timing information so the antenna computer
  knows how to read the data you're sending it. An endless,
  alternating pattern of 0, 1, 0, 1, 0, 1, 0, 1, 0, 1...." He trails
  off.

  You ask if the antenna can handle a clock signal at the frequency
  you would need to use for the data from the stars. "There's no way
  it can! The only antenna we've installed capable of that is on top
  of a top-secret Easter Bunny installation, and you're definitely
  not-" You hang up the phone.

  You've extracted the antenna's clock signal generation assembunny
  code (your puzzle input); it looks mostly compatible with code you
  worked on just recently.

  This antenna code, being a signal generator, uses one extra
  instruction:

    out x transmits x (either an integer or the value of a register)
    as the next value for the clock signal.

  The code takes a value (via register a) that describes the signal to
  generate, but you're not sure how it's used. You'll have to find the
  input to produce the right signal through experimentation.

  What is the lowest positive integer that can be used to initialize
  register a and cause the code to output a clock signal of 0, 1, 0,
  1... repeating forever?

cpy a d
cpy 9 c
cpy 282 b
inc d
dec b
jnz b -2
dec c
jnz c -5
cpy d a
jnz 0 0
cpy a b
cpy 0 a
cpy 2 c
jnz b 2
jnz 1 6
dec b
dec c
jnz c -4
inc a
jnz 1 -7
cpy 2 b
jnz c 2
jnz 1 4
dec b
dec c
jnz 1 -4
jnz 0 0
out b
jnz a -19
jnz 1 -21

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
import java.lang.Integer;
//import java.lang.Math;
import java.lang.String;
//import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
//import java.util.regex.Pattern;
//import java.util.regex.Matcher;

public class aoc_25 {

    public final static boolean __DEBUG__ = false;
    //public final static boolean __DEBUG__ = true;

    private static int max (final int a, final int b) { return a > b ? a : b; }

    private static int min (final int a, final int b) { return a < b ? a : b; }

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    public static void main (String[] args) throws Exception {
        String[] cmds = read_input(); if (cmds == null) return;
        //CBot[] cbs = parse_input (cmds); if (cbs == null) return;
        Assembunny asb;
        int p, q;
        for (q = -1,
                 p = args.length > 0 ? Integer.parseUnsignedInt (args[0]) : 0;
             q < 0 && p >= 0; ++p) {
            asb = new Assembunny (4);
            asb.reg[0] = p;
            q = asb.exec (cmds, 1024);
            //if (q < 0) System.out.printf ("Tested: %d : bad\n", p);
            if (q >= 0) {
                System.out.printf ("Tested: %d : ok\n", p);
                asb.print();
            }
        }
    }
}


class Assembunny {

    //public final static boolean __TRACE__ = true;
    public final static boolean __TRACE__ = false;

    int[] reg;
    int clk; // clock signal
    String[] cmds;

    private boolean csc;
    private int fcnt;

    Assembunny() {
        this (4);
    }

    Assembunny (final int nregf) {
        reg = new int[nregf];
        for (int i = 0; i < reg.length; ++i)
            reg[i] = 0;
        clk = -1;
        csc = clk2bool();
        fcnt = 0;
    }


    private boolean clk2bool() {
        return clk == 0 ? false : true;
    }


    public void print () {
        for (int i = 0; i < reg.length; ++i)
            System.out.printf ("R%d %d  ", i, reg[i]);
        System.out.printf ("CLK %d\n", clk);
    }


    public void print (final int ip) {
        System.out.printf ("IP %d  ", ip);
        print();
    }


    public void print_clk () {
        System.out.printf ("%d", clk);
    }


    private int reg_by_name (final String nreg) {
        switch (nreg.toUpperCase()) {
        case "A":
            return 0;
        case "B":
            return 1;
        case "C":
            return 2;
        case "D":
            return 3;
        }
        return -1;
    }


    private void asb_cpy (final String[] cmd) {
        int rd = reg_by_name (cmd[2]),
            rs = reg_by_name (cmd[1]);
        if (rs < 0)
            // int to reg
            rs = Integer.parseUnsignedInt (cmd[1]);
        else
            // reg to reg
            rs = reg[rs];
        reg[rd] = rs;
    }


    private void asb_inc (final String[] cmd) {
        int rn = reg_by_name (cmd[1]);
        ++reg[rn];
    }


    private void asb_dec (final String[] cmd) {
        int rn = reg_by_name (cmd[1]);
        --reg[rn];
    }


    private int asb_jnz (final String[] cmd) {
        int rs = reg_by_name (cmd[1]);
        if (rs < 0)
            rs = Integer.parseInt (cmd[1]);
        else
            rs = reg[rs];
        if (rs != 0)
            return Integer.parseInt (cmd[2]);
        return 1;
    }


    private int asb_out (final String[] cmd) {
        // out x ; transmits x (either an integer or the value of a
        // register) as the next value for the clock signal.
        int rs = reg_by_name (cmd[1]);
        if (rs < 0)
            // int
            rs = Integer.parseUnsignedInt (cmd[1]);
        else
            // reg
            rs = reg[rs];
        clk = rs;
        return rs;
    }


    private boolean check_state_change () {
        // checks CLK alteration
        // returns true iff state not changed
        boolean sc;
        sc = clk2bool();
        if (sc == csc) return true;
        csc = sc;
        ++fcnt;
        return false;
    }


    public int exec (final String[] _cmds, final int num_flips) {
        int i, p, q;
        long ic;
        String[] sc;
        cmds = _cmds;
        for (ic = 0, i = 0; i < cmds.length && ic >= 0; ++i, ++ic) {
            if (aoc_25.__DEBUG__) System.out.printf ("%d  %s ; ", i, cmds[i]);
            if (cmds[i].charAt(0) == ';') // NOP
                continue;
            sc = cmds[i].split (" +");

            switch (sc[0].toUpperCase()) {
            case "CPY":
                asb_cpy (sc);
                break;

            case "INC":
                asb_inc (sc);
                break;

            case "DEC":
                asb_dec (sc);
                break;

            case "JNZ":
                p = asb_jnz (sc);
                i += p - 1;
                if (__TRACE__) print (i);
                break;

            case "OUT":
                q = clk;
                p = asb_out (sc);
                //System.out.printf ("IC %d  CLK %d\n", ic, p);
                //print_clk();
                if (check_state_change() && q != -1) {
                    //System.out.println();
                    return -1;
                }
                else if (fcnt > num_flips) {
                    //System.out.println();
                    return 0;
                }
                break;

            default:
                System.out.printf ("err cmd: `%s`\n", cmds[i]);
                return -2;
            }

            if (aoc_25.__DEBUG__) print (i);
        }
        return 0;
    }
}

