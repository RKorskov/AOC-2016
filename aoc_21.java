// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-30 13:57:11 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  602214085774
  http://adventofcode.com/2016/day/21
  korskov.r.v@gmail.com

  --- Day 21: Scrambled Letters and Hash ---

  The computer system you're breaking into uses a weird scrambling
  function to store its passwords. It shouldn't be much trouble to
  create your own scrambled password so you can add it to the system;
  you just have to implement the scrambler.

  The scrambling function is a series of operations (the exact list is
  provided in your puzzle input). Starting with the password to be
  scrambled, apply each operation in succession to the string. The
  individual operations behave as follows:

    swap position X with position Y means that the letters at indexes
    X and Y (counting from 0) should be swapped.

    swap letter X with letter Y means that the letters X and Y should
    be swapped (regardless of where they appear in the string).

    rotate left/right X steps means that the whole string should be
    rotated; for example, one right rotation would turn abcd into
    dabc.

    rotate based on position of letter X means that the whole string
    should be rotated to the right based on the index of letter X
    (counting from 0) as determined before this instruction does any
    rotations. Once the index is determined, rotate the string to the
    right one time, plus a number of times equal to that index, plus
    one additional time if the index was at least 4.

    reverse positions X through Y means that the span of letters at
    indexes X through Y (including the letters at X and Y) should be
    reversed in order.

    move position X to position Y means that the letter which is at
    index X should be removed from the string, then inserted such that
    it ends up at index Y.

  For example, suppose you start with abcde and perform the following
  operations:

    swap position 4 with position 0 swaps the first and last letters,
    producing the input for the next step, ebcda.

    swap letter d with letter b swaps the positions of d and b: edcba.

    reverse positions 0 through 4 causes the entire string to be
    reversed, producing abcde.

    rotate left 1 step shifts all letters left one position, causing
    the first letter to wrap to the end of the string: bcdea.

    move position 1 to position 4 removes the letter at position 1
    (c), then inserts it at position 4 (the end of the string): bdeac.

    move position 3 to position 0 removes the letter at position 3
    (a), then inserts it at position 0 (the front of the string):
    abdec.

    rotate based on position of letter b finds the index of letter b
    (1), then rotates the string right once plus a number of times
    equal to that index (2): ecabd.

    rotate based on position of letter d finds the index of letter d
    (4), then rotates the string right once, plus a number of times
    equal to that index, plus an additional time because the index was
    at least 4, for a total of 6 right rotations: decab.

  After these steps, the resulting scrambled password is decab.

  Now, you just need to generate a new scrambled password and you can
  access the system. Given the list of scrambling operations in your
  puzzle input, what is the result of scrambling abcdefgh?

  (2)

  You scrambled the password correctly, but you discover that you
  can't actually modify the password file on the system. You'll need
  to un-scramble one of the existing passwords by reversing the
  scrambling process.

  What is the un-scrambled version of the scrambled password fbgdceah?
 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
import java.lang.Integer;
//import java.lang.Long;
//import java.lang.Math;
import java.lang.String;
//import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
//import java.util.regex.Pattern;
//import java.util.regex.Matcher;

public class aoc_21 {

    public final static boolean __DEBUG__ = false;
    //public final static boolean __DEBUG__ = true;
    //public final static boolean __DEBUG2__ = false;
    public final static boolean __DEBUG2__ = true;

    private static int max (final int a, final int b) { return a > b ? a : b; }

    private static int min (final int a, final int b) { return a < b ? a : b; }

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    private static CMD[] parse_input (String[] sdata) {
        ArrayList <CMD> ali = new ArrayList <CMD> ();
        for (String s: sdata) {
            ali.add (new CMD (s));
        }
        return ali.toArray (new CMD [ali.size()]);
    }


    private static void cswap (char[] src, int p, int q) {
        char c = src[q];
        src[q] = src[p];
        src[p] = c;
    }


    private static char[] cmd_rol (char[] src, final int op0) {
        /**
          rotate left X steps means that the whole string should
          be rotated; for example, one right rotation would turn abcd
          into dabc.
         */
        char[] rs;
        int i, p;
        rs = new char [src.length];
        for (i = 0, p = (src.length - op0) % src.length; i < src.length;
             ++i, ++p) {
            if (p >= src.length)
                p = 0;
            rs[p] = src[i];
        }

        for (i = 0; i < src.length; ++i)
            src[i] = rs[i];
        return rs;
    }


    private static void cmd_ror (char[] src, final int rep) {
        /**
          rotate right X steps means that the whole string should
          be rotated; for example, one right rotation would turn abcd
          into dabc.
         */
        char[] rs;
        int i, p;
        rs = new char [src.length];
        for (i = 0, p = rep % src.length; i < src.length;
             ++i, ++p) {
            if (p >= src.length)
                p = 0;
            rs[p] = src[i];
        }

        for (i = 0; i < src.length; ++i)
            src[i] = rs[i];
    }


    private static void cmd_rop (char[] src, final int op0) {
        /**
           rotate based on position of letter X means that the whole
           string should be rotated to the right based on the index of
           letter X (counting from 0) as determined before this
           instruction does any rotations. Once the index is
           determined, rotate the string to the right one time, plus a
           number of times equal to that index, plus one additional
           time if the index was at least 4.
           x>3 ? x+2 : x+1
         */
        int i, rep;
        for (i = 0, rep = -1; i < src.length; ++i)
            if (src[i] == (char) op0) {
                rep = i + ((i >= 4) ? 2 : 1);
                break;
            }
        if (rep > 0)
            cmd_ror (src, rep % src.length);
    }


    private static void cmd_swp (char[] src, final int op0, final int op1) {
        cswap (src, op0, op1);
    }


    private static void cmd_swl (char[] src, final int op0, final int op1) {
        /**
           swap letter X with letter Y means that the letters X and Y
           should be swapped (regardless of where they appear in the
           string).
         */
        // только первая пара от начала
        int i, p, q;
        char c;
        for (i = 0, p = q = -1; i < src.length && (p < 0 || q < 0); ++i) {
            if (p < 0 && src[i] == (char) op0)
                p = i;
            if (q < 0 && src[i] == (char) op1)
                q = i;
        }

        if (p >= 0 && q >= 0)
            cswap (src, p, q);
    }


    private static void cmd_mov (char[] src, final int op0, final int op1) {
        /**
           move position X to position Y means that the letter which
           is at index X should be removed from the string, then
           inserted such that it ends up at index Y.
         */
        char c;
        int i;
        c = src[op0];
        if (op0 < op1) {// x < y
            for (i = op0; i < op1; ++i)
                src[i] = src[i+1];
        }
        else {// y < x
            for (i = op0; i > op1 ; --i) {
                if (__DEBUG__)
                    System.out.printf ("move [%d], [%d]\n", i, i-1);
                src[i] = src[i-1];
            }
        }
        src[op1] = c;
    }


    private static void cmd_rev (char[] src, final int op0, final int op1) {
        /**
           reverse positions X through Y means that the span of
           letters at indexes X through Y (including the letters at X
           and Y) should be reversed in order.
         */
        int i, j;
        for (i = op0, j = op1; i < j; ++i, --j)
            cswap (src, i, j);
    }


    private static void cmd_rrp (char[] src, final int op0) {
        /**
           reverse version of ROP
           x>=4 ? x-2 : x-1
         */
        int i, rep;
        for (i = 0, rep = -1; i < src.length; ++i)
            if (src[i] == (char) op0) {
                /*
                  //rep = (i >= 4) ? (i+2) : (i+1);
                  0..3 ~> (1..4) % src.length
                  4..n ~> (6..n+2) % src.length
                 */
                if (i != 0 && i != 5)
                    rep = i < 5 ? i - 1 : i - 2;
                break;
            }
        if (rep > 0)
            cmd_rol (src, rep % src.length);
    }


    private static void cmd_rmv (char[] src, final int op0, final int op1) {
        /**
           reverse version of cmd_mov
           move position X to position Y means that the letter which
           is at index X should be removed from the string, then
           inserted such that it ends up at index Y.
         */
        char c;
        int i;
        c = src[op0];
        if (op0 < op1) {// x < y
            for (i = op0; i < op1; ++i)
                src[i] = src[i+1];
        }
        else {// y < x
            for (i = op0; i > op1 ; --i) {
                if (__DEBUG__)
                    System.out.printf ("move [%d], [%d]\n", i, i-1);
                src[i] = src[i-1];
            }
        }
        src[op1] = c;
    }


    private static void cmd_rrv (char[] src, final int op0, final int op1) {
        /**
           reverse version of cmd_rev
           reverse positions X through Y means that the span of
           letters at indexes X through Y (including the letters at X
           and Y) should be reversed in order.
         */
        int i, j;
        for (i = op0, j = op1; i < j; ++i, --j)
            cswap (src, i, j);
    }


    private static String do_exec (final CMD[] cmds, final String ssrc) {
        char[] pwd;
        pwd = ssrc.toCharArray();
        for (CMD cmd : cmds) {
            if (__DEBUG__) {
                System.out.println (new String (pwd));
                System.out.printf ("%s  %d  %d\n", cmd.opc, cmd.op0, cmd.op1);
            }
            switch (cmd.opc) {
            case ROP: {
                cmd_rop (pwd, cmd.op0);
                break;
            }

            case ROL: {
                cmd_rol (pwd, cmd.op0);
                break;
            }

            case ROR: {
                cmd_ror (pwd, cmd.op0);
                break;
            }

            case SWP: {
                cmd_swp (pwd, cmd.op0, cmd.op1);
                break;
            }

            case SWL: {
                cmd_swl (pwd, cmd.op0, cmd.op1);
                break;
            }

            case MOV: {
                cmd_mov (pwd, cmd.op0, cmd.op1);
                break;
            }

            case REV: {
                cmd_rev (pwd, cmd.op0, cmd.op1);
                break;
            }

            default:
                return null;
            }
        }
        return new String (pwd);
    }


    private static String do_rexec (final CMD[] cmds, final String dsrc) {
        char[] pwd;
        int i;
        CMD cmd;
        pwd = dsrc.toCharArray();
        for (i = cmds.length - 1; i >= 0; --i) {
            cmd = cmds[i];

            if (__DEBUG2__) {
                System.out.println (new String (pwd));
                System.out.printf ("%s  %d  %d\n", cmd.opc, cmd.op0, cmd.op1);
            }

            switch (cmd.opc) {
            case ROP: {
                cmd_rrp (pwd, cmd.op0);
                break;
            }

            case ROL: {
                cmd_ror (pwd, cmd.op0);
                break;
            }

            case ROR: {
                cmd_rol (pwd, cmd.op0);
                break;
            }

            case SWP: {
                cmd_swp (pwd, cmd.op0, cmd.op1);
                break;
            }

            case SWL: {
                cmd_swl (pwd, cmd.op0, cmd.op1);
                break;
            }

            case MOV: {
                //cmd_mov (pwd, cmd.op1, cmd.op0); // should work too
                cmd_rmv (pwd, cmd.op0, cmd.op1);
                break;
            }

            case REV: {
                cmd_rrv (pwd, cmd.op0, cmd.op1);
                break;
            }

            default:
                return null;
            }
        }
        return new String (pwd);
    }


    private static String do_exec
        (final CMD[] cmds, final String dsrc, final boolean xfdirect) {
        // xfdirect -- hash string
        // !xfdirect -- unhash string
        return xfdirect ? do_exec (cmds, dsrc) : do_rexec (cmds, dsrc);
    }


    public static void main (String[] args) throws Exception {
        final String ssrc, dsrc;
        ssrc = args.length > 0 ? args[0] : "abcdefgh";
        dsrc = args.length > 1 ? args[1] : "fbgdceah";
        String[] sdata = read_input(); if (sdata == null) return;
        CMD[] cmds = parse_input (sdata); if (cmds == null) return;
        String pwd = do_exec (cmds, ssrc, true);
        System.out.println (pwd);
        //pwd = do_exec (cmds, dsrc, false);
        pwd = do_exec (cmds, pwd, false);
        System.out.println (pwd);
    }
}


enum OPC {
    NOP (0),
    ROP (1), // rotate based on position of letter a
    ROR (2), // rotate right 2 steps
    ROL (3), // rotate left 3 steps
    SWP (4), // swap position 7 with position 1
    SWL (5), // swap letter g with letter d
    MOV (6), // move position 1 to position 5
    REV (7); // reverse positions 6 through 7

    private int opc; // opcode

    OPC (int n) {
        opc = n;
    }
}


class CMD {
    OPC opc;
    int op0, op1; // (move, reverse) | (swap, rotate based)

    CMD (String cs) {
        String[] sc = cs.split (" ");
        switch (sc[0].toUpperCase()) {
        case "ROTATE": {
            op1 = -1;
            switch (sc[1].toUpperCase()) {
            case "LEFT": {
                opc = OPC.ROL;
                op0 = Integer.parseUnsignedInt (sc[2]);
                break;
            }

            case "RIGHT": {
                opc = OPC.ROR;
                op0 = Integer.parseUnsignedInt (sc[2]);
                break;
            }

            case "BASED": {
                opc = OPC.ROP;
                op0 = (int) sc[6].charAt (0);
                break;
            }

            default:
                return;
            }
            break;
        }

        case "SWAP": {
            switch (sc[1].toUpperCase()) {
            case "POSITION": {
                opc = OPC.SWP;
                op0 = Integer.parseUnsignedInt (sc[2]);
                op1 = Integer.parseUnsignedInt (sc[5]);
                break;
            }

            case "LETTER": {
                opc = OPC.SWL;
                op0 = (int) sc[2].charAt (0);
                op1 = (int) sc[5].charAt (0);
                break;
            }

            default:
                return;
            }
            break;
        }

        case "MOVE": {
            opc = OPC.MOV;
            op0 = Integer.parseUnsignedInt (sc[2]);
            op1 = Integer.parseUnsignedInt (sc[5]);
            break;
        }

        case "REVERSE": {
            opc = OPC.REV;
            op0 = Integer.parseUnsignedInt (sc[2]);
            op1 = Integer.parseUnsignedInt (sc[4]);
            break;
        }

        default:
            return;
        }
    }
}
