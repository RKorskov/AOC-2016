// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-23 12:55:08 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  http://adventofcode.com/2016/day/15

  t=0.., n=1..
  (t+n + 4) % 5 = 0
  (t+n + 1) % 2 = 0

  --- Day 15: Timing is Everything ---

  The halls open into an interior plaza containing a large kinetic
  sculpture. The sculpture is in a sealed enclosure and seems to
  involve a set of identical spherical capsules that are carried to
  the top and allowed to bounce through the maze of spinning pieces.

  Part of the sculpture is even interactive! When a button is pressed,
  a capsule is dropped and tries to fall through slots in a set of
  rotating discs to finally go through a little hole at the bottom and
  come out of the sculpture. If any of the slots aren't aligned with
  the capsule as it passes, the capsule bounces off the disc and soars
  away. You feel compelled to get one of those capsules.

  The discs pause their motion each second and come in different
  sizes; they seem to each have a fixed number of positions at which
  they stop. You decide to call the position with the slot 0, and
  count up for each position it reaches next.

  Furthermore, the discs are spaced out so that after you push the
  button, one second elapses before the first disc is reached, and one
  second elapses as the capsule passes from one disc to the one below
  it. So, if you push the button at time=100, then the capsule reaches
  the top disc at time=101, the second disc at time=102, the third
  disc at time=103, and so on.

  The button will only drop a capsule at an integer time - no
  fractional seconds allowed.

  For example, at time=0, suppose you see the following arrangement:

  Disc #1 has 5 positions; at time=0, it is at position 4.
  Disc #2 has 2 positions; at time=0, it is at position 1.

  If you press the button exactly at time=0, the capsule would start
  to fall; it would reach the first disc at time=1. Since the first
  disc was at position 4 at time=0, by time=1 it has ticked one
  position forward. As a five-position disc, the next position is 0,
  and the capsule falls through the slot.

  Then, at time=2, the capsule reaches the second disc. The second
  disc has ticked forward two positions at this point: it started at
  position 1, then continued to position 0, and finally ended up at
  position 1 again. Because there's only a slot at position 0, the
  capsule bounces away.

  If, however, you wait until time=5 to push the button, then when the
  capsule reaches each disc, the first disc will have ticked forward
  5+1 = 6 times (to position 0), and the second disc will have ticked
  forward 5+2 = 7 times (also to position 0). In this case, the
  capsule would fall through the discs and come out of the machine.

  However, your situation has more than two discs; you've noted their
  positions in your puzzle input. What is the first time you can press
  the button to get a capsule?

Disc #1 has 17 positions; at time=0, it is at position 15.
Disc #2 has 3 positions; at time=0, it is at position 2.
Disc #3 has 19 positions; at time=0, it is at position 4.
Disc #4 has 13 positions; at time=0, it is at position 2.
Disc #5 has 7 positions; at time=0, it is at position 2.
Disc #6 has 5 positions; at time=0, it is at position 0.

(t+1+15)%17=0
(t+2+2)%3=0
(t+3+4)%19=0
(t+4+2)%13=0
(t+5+2)%7=0
(t+6+0)%5=0

(t+16)%17=0
(t+4)%3=0
(t+7)%19=0
(t+6)%13=0
(t+7)%7=0
(t+6)%5=0

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
//import java.lang.Integer;
//import java.lang.Long;
//import java.lang.Math;
import java.lang.String;
import java.lang.StringBuilder;
import java.security.MessageDigest;
//import java.text.DateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Scanner; // Scanner (System.in);
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class aoc_15 {

    public final static boolean __DEBUG__ = false;
    //public final static boolean __DEBUG__ = true;

    /*
    private static int max (final int a, final int b) { return a > b ? a : b; }

    private static int min (final int a, final int b) { return a < b ? a : b; }

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }
    */




    public static void main (String[] args) throws Exception {
        final String salt;
        salt = args.length > 0 ? args[0] : "ahsbgdzn";
        //String[] cmds = read_input(); if (cmds == null) return;
        //get_keys (salt);
        //System.out.println (pwgen ("abc", 0, 0));
        //System.out.println (pwgen ("abc", 0, 2016));
        int t, p;
        for (t=0; t>=0; ++t) {
            p = (t+16)%17; // 1
            p += (t+4)%3; // 2
            p += (t+7)%19; // 3
            p += (t+6)%13; // 4
            p += (t+7)%7; // 5
            p += (t+6)%5; // 6

            p += (t+7)%11; // 7

            /*
            p = (t+5)%5;
            p += (t+3)%2;
            */

            if (p == 0) {
                System.out.println (t);
                return;
            }
        }
    }
}

