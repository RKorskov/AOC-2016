// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-27 15:38:41 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  http://adventofcode.com/2016/day/19

  --- Day 19: An Elephant Named Joseph ---

  The Elves contact you over a highly secure emergency channel. Back
  at the North Pole, the Elves are busy misunderstanding White
  Elephant parties.

  Each Elf brings a present. They all sit in a circle, numbered
  starting with position 1. Then, starting with the first Elf, they
  take turns stealing all the presents from the Elf to their left. An
  Elf with no presents is removed from the circle and does not take
  turns.

  For example, with five Elves (numbered 1 to 5):

  1
5   2
 4 3

    Elf 1 takes Elf 2's present.
    Elf 2 has no presents and is skipped.
    Elf 3 takes Elf 4's present.
    Elf 4 has no presents and is also skipped.
    Elf 5 takes Elf 1's two presents.
    Neither Elf 1 nor Elf 2 have any presents, so both are skipped.
    Elf 3 takes Elf 5's three presents.

  So, with five Elves, the Elf that sits starting in position 3 gets
  all the presents.

  With the number of Elves given in your puzzle input, which Elf gets
  all the presents?

  Your puzzle input is 3012210.

  --- Part Two ---

  Realizing the folly of their present-exchange rules, the Elves agree
  to instead steal presents from the Elf directly across the
  circle. If two Elves are across the circle, the one on the left
  (from the perspective of the stealer) is stolen from. The other
  rules remain unchanged: Elves with no presents are removed from the
  circle entirely, and the other elves move in slightly to keep the
  circle evenly spaced.

  For example, with five Elves (again numbered 1 to 5):

    The Elves sit in a circle; Elf 1 goes first:

      1
    5   2
     4 3

    Elves 3 and 4 are across the circle; Elf 3's present is stolen,
    being the one to the left. Elf 3 leaves the circle, and the rest
    of the Elves move in:

      1           1
    5   2  -->  5   2
     4 -          4

    Elf 2 steals from the Elf directly across the circle, Elf 5:

      1         1 
    -   2  -->     2
      4         4 

    Next is Elf 4 who, choosing between Elves 1 and 2, steals from Elf 1:

     -          2  
        2  -->
     4          4

    Finally, Elf 2 steals from Elf 4:

     2
        -->  2  
     -

  So, with five Elves, the Elf that sits starting in position 2 gets
  all the presents.

  With the number of Elves given in your puzzle input, which Elf now
  gets all the presents?

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
//import java.lang.Integer;
//import java.lang.Long;
//import java.lang.Math;
import java.lang.String;
//import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Scanner; // Scanner (System.in);
//import java.util.regex.Pattern;
//import java.util.regex.Matcher;

public class aoc_19 {

    public final static boolean __DEBUG__ = false;
    //public final static boolean __DEBUG__ = true;
    //public final static boolean __TRACE__ = false;
    public final static boolean __TRACE__ = true;

    /*
    private static int max (final int a, final int b) { return a > b ? a : b; }

    private static int min (final int a, final int b) { return a < b ? a : b; }

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }
    */

    private static int loser (final int[] pres, final int pos) {
        int i;
        for (i = pos + 1; ; ++i) {
            if (i >= pres.length)
                i = 0;
            if (pres[i] > 0)
                break;
        }
        return i;
    }


    private static int winner (final int[] pres) {
        int i;
        for (i = 0; i < pres.length && pres[i] == 0; ++i);
        return i;
    }


    private static int stealing (final int[] pres) {
        int i, j;
        for (i = 0; i < pres.length; ++i) {
            if (pres[i] > 0) {
                j = loser (pres, i);
                pres[i] += pres[j];
                pres[j] = 0;
            }
        }
        return 0;
    }


    private static int welf (final int rep) {
        // part 1
        int i, cnt, j;
        int[] pres;
        pres = new int [rep];
        for (i = 0; i < rep; pres[i] = 1, ++i);
        for (cnt = rep, j = 0; pres [j] < rep;) {
            stealing (pres);
            j = winner (pres);
        }
        return j;
    }


    private static int cinc (final int[] pres, final int pos) {
        // cyclic increment
        int p;
        for (p = pos + 1; p != pos ; ++p) {
            if (p >= pres.length)
                p = 0;
            if (pres[p] > 0)
                break;
        }
        if (p == pos)
            return -1;
        return p;
    }


    private static int cdec (final int[] pres, final int pos) {
        // cyclic decrement
        int p;
        for (p = pos - 1; p != pos ; --p) {
            if (p < 0)
                p = pres.length - 1;
            if (pres[p] > 0)
                break;
        }
        if (p == pos)
            return -1;
        return p;
    }


    private static int closer (final int[] pres, final int pos) {
        // loser in circle
        int tl, tr, ptl, ptr;
        for (ptl = pos, ptr = pos, tl = -1, tr = -2 ;
             ; // tl != ptl || tr != ptr
             ptl = tl, ptr = tr) {
            tl = cdec (pres, ptl);
            tr = cinc (pres, ptr);

            if (ptl == tr && ptr == tl) // odd membres
                break;

            if (tl == pos || tl == ptr)
                tl = ptl;
            if (tr == pos || tr == ptl)
                tr = ptr;

            if (tr == tl) // even members
                break;

            if (__DEBUG__)
                System.out.printf ("pL%d  L%d  pR%d  R%d\n", ptl, tl, ptr, tr);
        }

        if (tl == pos)
            return tr;
        if (tr == pos)
            return tl;

        return tl;
    }

    private static long SCnt = 0;
    private static int cstealing (final int[] pres) {
        // stealing by circle
        // same as looser, but uses closer
        int cn = 0;
        int i, j;
        for (i = 0; i < pres.length; ++i) {
            if (__DEBUG__) System.out.printf ("S:%d\n", ++cn);
            if (pres[i] > 0) {
                j = closer (pres, i);
                if (i == j || j == -1)
                    return i;
                if (__TRACE__) {
                    ++SCnt;
                    if (SCnt >= 1024) {
                        System.out.printf ("steal %d from %d\n", i+1, j+1);
                        SCnt = 0;
                    }
                }
                pres[i] += pres[j];
                pres[j] = 0;
            }
            if (__DEBUG__) eprint (pres);
        }
        return 0;
    }


    private static void eprint (final int[] prs) {
        int i;
        for (i = 0; i < prs.length; ++i)
            System.out.printf ("%d  ", prs[i]);
        System.out.println();
    }


    private static int celf (final int rep) {
        // part 2
        int i, cnt, j;
        int[] pres;
        pres = new int [rep];
        for (i = 0; i < rep; pres[i] = 1, ++i);
        //if (__TRACE__) eprint (pres);
        for (cnt = rep, j = 0; pres [j] < rep;) {
            j = cstealing (pres);
            //if (__TRACE__) eprint (pres);
            if (j != 0)
                return j;
            //j = winner (pres);
        }
        return j;
    }


    public static void main (String[] args) throws Exception {
        //final String seed;
        final int rep;
        int cnt;
        //seed = args.length > 0 ? args[0] : "......^.^^.....^^^^^^^^^...^.^..^^.^^^..^.^..^.^^^.^^^^..^^.^.^.....^^^^^..^..^^^..^^.^.^..^^..^^^..";
        //rep = args.length > 1 ? Integer.parseUnsignedInt (args[1]) : 40;
        rep = args.length > 0 ? Integer.parseUnsignedInt (args[0]) : 3012210;
        //String[] cmds = read_input(); if (cmds == null) return;
        cnt = welf (rep);
        System.out.println (cnt+1);
        cnt = celf (rep);
        System.out.println (cnt+1);
    }
}

