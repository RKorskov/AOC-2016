// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-12 17:40:59 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  You arrive at Easter Bunny Headquarters under cover of
  darkness. However, you left in such a rush that you forgot to use
  the bathroom! Fancy office buildings like this one usually have
  keypad locks on their bathrooms, so you search the front desk for
  the code.

  "In order to improve security," the document you find says,
  "bathroom codes will no longer be written down. Instead, please
  memorize and follow the procedure below to access the bathrooms."

  The document goes on to explain that each button to be pressed can
  be found by starting on the previous button and moving to adjacent
  buttons on the keypad: U moves up, D moves down, L moves left, and R
  moves right. Each line of instructions corresponds to one button,
  starting at the previous button (or, for the first line, the "5"
  button); press whatever button you're on at the end of each line. If
  a move doesn't lead to a button, ignore it.

  You can't hold it much longer, so you decide to figure out the code
  as you walk to the bathroom. You picture a keypad like this:

  1 2 3
  4 5 6
  7 8 9

  Suppose your instructions are:

  ULL
  RRDDD
  LURDL
  UUUUD

    You start at "5" and move up (to "2"), left (to "1"), and left
    (you can't, and stay on "1"), so the first button is 1.

    Starting from the previous button ("1"), you move right twice (to
    "3") and then down three times (stopping at "9" after two moves
    and ignoring the third), ending up with 9.

    Continuing from "9", you move left, up, right, down, and left,
    ending with 8.

    Finally, you move up four times (stopping at "2"), then down once,
    ending with 5.

  So, in this example, the bathroom code is 1985.

  Your puzzle input is the instructions from the document you found at
  the front desk. What is the bathroom code?

  --------------------------------

  You finally arrive at the bathroom (it's a several minute walk from
  the lobby so visitors can behold the many fancy conference rooms and
  water coolers on this floor) and go to punch in the code. Much to
  your bladder's dismay, the keypad is not at all like you imagined
  it. Instead, you are confronted with the result of hundreds of
  man-hours of bathroom-keypad-design meetings:

      1
    2 3 4
  5 6 7 8 9
    A B C
      D

  You still start at "5" and stop when you're at an edge, but given
  the same instructions as above, the outcome is very different:

    You start at "5" and don't move at all (up and left are both
    edges), ending at 5.

    Continuing from "5", you move right twice and down three times
    (through "6", "7", "B", "D", "D"), ending at D.

    Then, from "D", you move five more times (through "D", "B", "C",
    "C", "B"), ending at B.

    Finally, after five more moves, you end at 3.

  So, given the actual keypad layout, the code would be 5DB3.

  Using the same instructions in your puzzle input, what is the
  correct bathroom code?

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
//import java.lang.Integer;
//import java.lang.Math;
//import java.lang.String;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
//import java.util.regex.Pattern;
//import java.util.regex.Matcher;

public class aoc_02 {

    public static boolean __DEBUG__ = false;

    /*
      . . 1 . .
      . 2 3 4 .
      5 6 7 8 9
      . A B C .
      . . D . .
     */
    final static int rhpad[][] = {
        {0,0,1,0,0},
        {0,2,3,4,0},
        {5,6,7,8,9},
        {0,10,11,12,0},
        {0,0,13,0,0}
    };

    private static String[] read_input () {
        String[] strs;
        ArrayList <String> als = new ArrayList <String> ();
        for (Scanner sc = new Scanner (System.in);
             sc.hasNext();)
            als.add ((sc.next()).toUpperCase());
        strs = new String [als.size()];
        return als.toArray (strs);
    }


    private static int walk_pad3 (int pst, String cmd) {
        // walks over 3x3 pad according to command string,
        // starting from button pst
        int x, y, n, i;
        char c;
        x = (pst - 1) % 3;
        y = (pst - 1) / 3;
        //System.out.printf ("> %d %d\n", x, y);
        for (i = 0, n = cmd.length(); i < n; ++i) {
            c = cmd.charAt (i);
            //System.out.print (c);
            switch (c) {
            case 'U': {
                --y;
                break;
            }

            case 'D': {
                ++y;
                break;
            }

            case 'L': {
                --x;
                break;
            }

            case 'R': {
                ++x;
                break;
            }

            default:
                return -1;
            }

            if (x < 0)
                x = 0;
            if (x > 2)
                x = 2;
            if (y < 0)
                y = 0;
            if (y > 2)
                y = 2;
        }
        //System.out.printf ("\n< %d %d\n", x, y);
        return y * 3 + x + 1;
    }


    private static long walk_3x3 (String[] cmds) {
        int i, n;
        long rc;
        for (rc = 0, n = 5, i = 0; i < cmds.length; ++i) {
            n = walk_pad3 (n, cmds[i]);
            rc = rc * 10 + n;
        }
        return rc;
    }


    private static int walk_pad_rh (int pst, String cmd) {
        // walks over rhombus pad according to command string,
        // starting from button pst
        int x, y, n, i, j, nx, ny;
        final int XM, YM;
        char c;
        XM = rhpad[0].length;
        YM = rhpad.length;
        for (x = y = -1, i = 0; i < YM; ++i)
            for (j = 0; j < XM; ++j)
                if (rhpad[i][j] == pst) {
                    x = j;
                    y = i;
                    break;
                }
        if (x == -1)
            return -1;

        if (__DEBUG__) System.out.printf ("> %d %d\n", x, y);
        for (i = 0, n = cmd.length(); i < n; ++i) {
            c = cmd.charAt (i);
            if (__DEBUG__) System.out.print (c);
            nx = x;
            ny = y;
            switch (c) {
            case 'U': {
                --ny;
                break;
            }

            case 'D': {
                ++ny;
                break;
            }

            case 'L': {
                --nx;
                break;
            }

            case 'R': {
                ++nx;
                break;
            }

            default:
                return -1;
            }

            if (__DEBUG__) System.out.printf (" %d %d %d %d", x, y, nx, ny);
            if (nx < 0)
                nx = 0;
            if (nx >= XM)
                nx = YM - 1;
            if (ny < 0)
                ny = 0;
            if (ny >= YM)
                ny = YM - 1;

            if (__DEBUG__) System.out.printf ("  %d\n", rhpad[ny][nx]);
            if (rhpad[ny][nx] < 1) {
                nx = x;
                ny = y;
            }
            else {
                x = nx;
                y = ny;
            }
        }
        if (__DEBUG__) System.out.printf ("\n< %d %d\n", x, y);
        return rhpad[y][x];
    }


    private static long walk_rh (String[] cmds) {
        // rhombus pad
        int i, n;
        long rc;
        for (rc = 0, n = 5, i = 0; i < cmds.length; ++i) {
            n = walk_pad_rh (n, cmds[i]);
            rc = (rc << 4) + n;
        }
        return rc;
    }


    public static void main (String[] args) throws Exception {
        long c3, cr;
        String[] cmds = read_input();
        // 1st part
        c3 = walk_3x3 (cmds);
        System.out.println (c3);
        // 2nd part
        cr = walk_rh (cmds);
        System.out.printf ("%X\n", cr);
    }
}

