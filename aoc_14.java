// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-23 15:07:19 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  http://adventofcode.com/2016/day/14
  https://www.tutorialspoint.com/java/java_multithreading.htm
  https://www.tutorialspoint.com/java/java_thread_communication.htm

  --- Day 14: One-Time Pad ---

  In order to communicate securely with Santa while you're on this
  mission, you've been using a one-time pad that you generate using a
  pre-agreed algorithm. Unfortunately, you've run out of keys in your
  one-time pad, and so you need to generate some more.

  To generate keys, you first get a stream of random data by taking
  the MD5 of a pre-arranged salt (your puzzle input) and an increasing
  integer index (starting with 0, and represented in decimal); the
  resulting MD5 hash should be represented as a string of lowercase
  hexadecimal digits.

  However, not all of these MD5 hashes are keys, and you need 64 new
  keys for your one-time pad. A hash is a key only if:

    It contains three of the same character in a row, like 777. Only
    consider the first such triplet in a hash.

    One of the next 1000 hashes in the stream contains that same
    character five times in a row, like 77777.

  Considering future hashes for five-of-a-kind sequences does not
  cause those hashes to be skipped; instead, regardless of whether the
  current hash is a key, always resume testing for keys starting with
  the very next hash.

  For example, if the pre-arranged salt is abc:

    The first index which produces a triple is 18, because the MD5
    hash of abc18 contains ...cc38887a5.... However, index 18 does not
    count as a key for your one-time pad, because none of the next
    thousand hashes (index 19 through index 1018) contain 88888.

    The next index which produces a triple is 39; the hash of abc39
    contains eee. It is also the first key: one of the next thousand
    hashes (the one at index 816) contains eeeee.

    None of the next six triples are keys, but the one after that, at
    index 92, is: it contains 999 and index 200 contains 99999.

    Eventually, index 22728 meets all of the criteria to generate the
    64th key.

  So, using our example salt of abc, index 22728 produces the 64th
  key.

  Given the actual salt in your puzzle input, what index produces your
  64th one-time pad key?

  Your puzzle input is ahsbgdzn.

  --- Part Two ---

  Of course, in order to make this process even more secure, you've
  also implemented key stretching.

  Key stretching forces attackers to spend more time generating
  hashes. Unfortunately, it forces everyone else to spend more time,
  too.

  To implement key stretching, whenever you generate a hash, before
  you use it, you first find the MD5 hash of that hash, then the MD5
  hash of that hash, and so on, a total of 2016 additional
  hashings. Always use lowercase hexadecimal representations of
  hashes.

  For example, to find the stretched hash for index 0 and salt abc:

    Find the MD5 hash of abc0: 577571be4de9dcce85a041ba0410f29f.

    Then, find the MD5 hash of that hash: eec80a0c92dc8a0777c619d9bb51e910.

    Then, find the MD5 hash of that hash: 16062ce768787384c81fe17a7a60c7e3.

    ...repeat many times...

    Then, find the MD5 hash of that hash: a107ff634856bb300138cac6568c0f24.

  So, the stretched hash for index 0 in this situation is
  a107ff.... In the end, you find the original hash (one use of MD5),
  then find the hash-of-the-previous-hash 2016 times, for a total of
  2017 uses of MD5.

  The rest of the process remains the same, but now the keys are
  entirely different. Again for salt abc:

    The first triple (222, at index 5) has no matching 22222 in the
    next thousand hashes.

    The second triple (eee, at index 10) hash a matching eeeee at
    index 89, and so it is the first key.

    Eventually, index 22551 produces the 64th key (triple fff with
    matching fffff at index 22859.

  Given the actual salt in your puzzle input and using 2016 extra MD5
  calls of key stretching, what index now produces your 64th one-time
  pad key?

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
//import java.lang.Integer;
import java.lang.Long;
//import java.lang.Math;
import java.lang.String;
import java.lang.StringBuilder;
import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.HashMap;
//import java.util.Scanner; // Scanner (System.in);
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class aoc_14 {

    public final static boolean __DEBUG__ = false;
    //public final static boolean __DEBUG__ = true;

    /*
    private static int max (final int a, final int b) { return a > b ? a : b; }

    private static int min (final int a, final int b) { return a < b ? a : b; }

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }
    */


    private static String pwgen (final String salt, final long idx)
        throws Exception {
        MessageDigest md5;
        byte[] ds, dd;
        String s;
        md5 = MessageDigest.getInstance ("MD5");
        s = String.format ("%s%d", salt, idx);
        ds = s.getBytes();
        md5.update (ds);
        dd = md5.digest();
        return bytes2str (dd);
    }


    public static String pwgen
        (final String salt, final long idx, final int rep)
        throws Exception {
        // key stretching
        MessageDigest md5;
        byte[] ds;
        String s;
        int i;
        md5 = MessageDigest.getInstance ("MD5");
        s = String.format ("%s%d", salt, idx);
        for (i = 0; i <= rep; ++i) {
            ds = s.getBytes();
            ds = md5.digest(ds);
            s = bytes2str (ds);
        }
        return s;
    }


    private static String bytes2str (byte[] bb) {
        StringBuilder ds = new StringBuilder ();
        String s;
        int i;
        for (i = 0; i < bb.length; ++i) {
            s = String.format ("%02x", bb[i]);
            ds.append (s);
        }
        return ds.toString();
    }


    private static long find3d (final String salt, final long sidx)
        throws Exception {
        String s;
        long idx;
        Matcher d3m;
        Pattern d3p = Pattern.compile ("(.)\\1\\1");

        for (idx = sidx; idx >= 0; ++idx) {
            s = pwgen (salt, idx);
            d3m = d3p.matcher (s);
            if (d3m.find())
                return idx;
        }
        return -1;
    }


    private static CIDX find3d
        (final String salt, final long sidx, final int rep)
        throws Exception {
        // key stretching by rep
        String s;
        long idx;
        Matcher d3m;
        Pattern d3p = Pattern.compile ("(.)\\1\\1");
        int i;

        for (i = 0, idx = sidx; idx >= 0; ++idx, ++i) {
            s = pwgen (salt, idx, rep);
            d3m = d3p.matcher (s);
            if (d3m.find())
                return new CIDX (idx, d3m.group(1).charAt(0));

            if (i >= 16) {
                if (i >= 64) {
                    i = 0;
                    System.out.print ('-');
                }
                else
                    System.out.print ('_');
            }
        }
        return null;
    }


    private static CIDX find3d
        (HashMap <Long, String> hms,
         final String salt, final long sidx, final int rep)
        throws Exception {
        // key stretching by rep
        String s;
        long idx;
        Matcher d3m;
        Pattern d3p = Pattern.compile ("(.)\\1\\1");

        for (idx = sidx; idx >= 0; ++idx) {
            if (hms.containsKey (idx))
                s = hms.get (idx);
            else {
                s = pwgen (salt, idx, rep);
                hms.put (idx, s);
            }
            d3m = d3p.matcher (s);
            if (d3m.find())
                return new CIDX (idx, d3m.group(1).charAt(0));
        }
        return null;
    }


    private static long find4d
        (final String salt, final long sidx, final char dgt)
        throws Exception{
        long idx;
        int i;
        String s;
        final String d4s = new String (new char[] {dgt, dgt, dgt, dgt, dgt});

        for (idx = sidx; idx >= 0 && (idx - sidx) < 1000; ++idx) {
            s = pwgen (salt, idx);
            i = s.indexOf (d4s);
            if (i >= 0)
                return idx;
        }
        return -1;
    }


    private static long find4d
        (HashMap <Long, String> hms,
         final String salt, final long sidx, final char dgt, final int rep)
        throws Exception{
        // key stretching by rep
        long idx;
        int i, j;
        String s;
        final String d4s = new String (new char[] {dgt, dgt, dgt, dgt, dgt});

        for (j = 0, idx = sidx; idx >= 0 && (idx - sidx) < 1000; ++idx, ++j) {
            if (hms.containsKey (idx))
                s = hms.get (idx);
            else {
                s = pwgen (salt, idx, rep);
                hms.put (idx, s);
            }
            i = s.indexOf (d4s);
            if (i >= 0)
                return idx;
        }
        return -1;
    }


    public static void get_keys (final String salt) throws Exception {
        long d3idx, d4idx;
        CIDX cidx;
        int kcnt;
        for (kcnt = 0, d3idx = 0; d3idx >= 0 && kcnt < 64; ++d3idx) {
            cidx = find3d (salt, d3idx, 0);
            if (cidx == null) {
                System.out.println ("no more keys");
                return;
            }
            d3idx = cidx.idx;
            d4idx = find4d (salt, d3idx + 1, cidx.ch);
            if (d4idx > 0) {
                ++kcnt;
                System.out.printf
                    ("%d  %d\n", kcnt, d3idx);
                //System.out.printf ("\t%d  %c  '%s'\n", d4idx, cidx.ch, pwgen (salt, d4idx));
            }
        }
    }


    public static void get_keys
        (HashMap <Long, String> hms, final String salt, final int rep)
        throws Exception {
        // stretched keys
        long d3idx, d4idx;
        CIDX cidx;
        int kcnt;
        //String s;
        char c;

        for (kcnt = 0, d3idx = 0; d3idx >= 0 && kcnt < 64; ++d3idx) {
            cidx = find3d (hms, salt, d3idx, rep);
            if (cidx == null)
                return;
            d3idx = cidx.idx;
            c = cidx.ch;
            d4idx = find4d (hms, salt, d3idx + 1, c, rep);
            if (d4idx > 0) {
                ++kcnt;
                System.out.printf
                    ("%d  %d  '%s'\n", kcnt, d3idx, hms.get (d3idx));
                System.out.printf ("\t%d  %c  '%s'\n",
                                   d4idx, c, hms.get (d4idx));
            }
        }
    }


    public static void main (String[] args) throws Exception {
        final String salt;
        salt = args.length > 0 ? args[0] : "ahsbgdzn";
        System.out.printf ("salt : '%s'\n", salt);
        //String[] cmds = read_input(); if (cmds == null) return;
        //get_keys (salt);
        //System.out.println (pwgen ("abc", 0, 0));
        //System.out.println (pwgen ("abc", 0, 2016));
        HashMap <Long, String> hms = new HashMap <Long, String> ();
        //System.out.println (pwgen ("abc", 22551, 2016));
        //System.out.println (pwgen ("abc", 22859, 2016));
        get_keys (hms, salt, 2016);
    }
}


class CIDX {
    final long idx;
    final char ch;

    CIDX (long i, char c) {
        idx = i;
        ch = c;
    }
}
