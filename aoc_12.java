// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-22 11:27:16 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  http://adventofcode.com/2016/day/12

  --- Day 12: Leonardo's Monorail ---

  You finally reach the top floor of this building: a garden with a
  slanted glass ceiling. Looks like there are no more stars to be had.

  While sitting on a nearby bench amidst some tiger lilies, you manage
  to decrypt some of the files you extracted from the servers
  downstairs.

  According to these documents, Easter Bunny HQ isn't just this
  building - it's a collection of buildings in the nearby
  area. They're all connected by a local monorail, and there's another
  building not far from here! Unfortunately, being night, the monorail
  is currently not operating.

  You remotely connect to the monorail control systems and discover
  that the boot sequence expects a password. The password-checking
  logic (your puzzle input) is easy to extract, but the code it uses
  is strange: it's assembunny code designed for the new computer you
  just assembled. You'll have to execute the code and get the
  password.

  The assembunny code you've extracted operates on four registers (a,
  b, c, and d) that start at 0 and can hold any integer. However, it
  seems to make use of only a few instructions:

    cpy x y copies x (either an integer or the value of a register)
    into register y.

    inc x increases the value of register x by one.

    dec x decreases the value of register x by one.

    jnz x y jumps to an instruction y away (positive means forward;
    negative means backward), but only if x is not zero.

    The jnz instruction moves relative to itself: an offset of -1
    would continue at the previous instruction, while an offset of 2
    would skip over the next instruction.

  For example:

cpy 41 a
inc a
inc a
dec a
jnz a 2
dec a

  The above code would set register a to 41, increase its value by 2,
  decrease its value by 1, and then skip the last dec a (because a is
  not zero, so the jnz a 2 skips it), leaving register a at 42. When
  you move past the last instruction, the program halts.

  After executing the assembunny code in your puzzle input, what value
  is left in register a?

  --- Part Two ---

  As you head down the fire escape to the monorail, you notice it
  didn't start; register c needs to be initialized to the position of
  the ignition key.

  If you instead initialize register c to be 1, what value is now left
  in register a?


 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
import java.lang.Integer;
//import java.lang.Math;
import java.lang.String;
//import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
//import java.util.regex.Pattern;
//import java.util.regex.Matcher;

public class aoc_12 {

    public final static boolean __DEBUG__ = false;
    //public final static boolean __DEBUG__ = true;

    private static int max (final int a, final int b) { return a > b ? a : b; }

    private static int min (final int a, final int b) { return a < b ? a : b; }

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    public static void main (String[] args) throws Exception {
        String[] cmds = read_input(); if (cmds == null) return;
        //CBot[] cbs = parse_input (cmds); if (cbs == null) return;
        Assembunny asb = new Assembunny (4);
        asb.exec (cmds);
        asb.print();
    }
}


class Assembunny {
    int[] reg;
    String[] cmds;

    Assembunny() {
        this (4);
    }

    Assembunny (int nregf) {
        reg = new int[nregf];
        for (int i = 0; i < reg.length; ++i)
            reg[i] = 0;
    }


    public void print () {
        for (int i = 0; i < reg.length; ++i)
            System.out.printf ("%d  ", reg[i]);
        System.out.println();
    }


    private int reg_by_name (String nreg) {
        switch (nreg.toUpperCase()) {
        case "A":
            return 0;
        case "B":
            return 1;
        case "C":
            return 2;
        case "D":
            return 3;
        }
        return -1;
    }


    private void asb_cpy (String[] cmd) {
        int rd = reg_by_name (cmd[2]),
            rs = reg_by_name (cmd[1]);
        if (rs < 0)
            // int to reg
            rs = Integer.valueOf (cmd[1]);
        else
            // reg to reg
            rs = reg[rs];
        reg[rd] = rs;
    }


    private void asb_inc (String[] cmd) {
        int rn = reg_by_name (cmd[1]);
        ++reg[rn];
    }


    private void asb_dec (String[] cmd) {
        int rn = reg_by_name (cmd[1]);
        --reg[rn];
    }


    private int asb_jnz (String[] cmd) {
        int rs = reg_by_name (cmd[1]);
        if (rs < 0)
            rs = Integer.valueOf (cmd[1]);
        else
            rs = reg[rs];
        if (rs != 0)
            return Integer.valueOf (cmd[2]);
        return 1;
    }


    public void exec (String[] _cmds) {
        int i;
        String[] sc;
        cmds = _cmds;
        for (i = 0; i < cmds.length; ++i) {
            if (aoc_12.__DEBUG__) System.out.printf ("%d  %s ; ", i, cmds[i]);
            if (cmds[i].charAt(0) == ';') // NOP
                continue;
            sc = cmds[i].split (" +");

            switch (sc[0].toUpperCase()) {
            case "CPY":
                asb_cpy (sc);
                break;

            case "INC":
                asb_inc (sc);
                break;

            case "DEC":
                asb_dec (sc);
                break;

            case "JNZ":
                i += asb_jnz (sc) - 1;
                break;

            default:
                System.out.printf ("err cmd: `%s`\n", cmds[i]);
                return;
            }

            if (aoc_12.__DEBUG__) print();
        }
    }
}

