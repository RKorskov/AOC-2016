// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-12 15:51:34 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  Santa's sleigh uses a very high-precision clock to guide its
  movements, and the clock's oscillator is regulated by
  stars. Unfortunately, the stars have been stolen... by the Easter
  Bunny. To save Christmas, Santa needs you to retrieve all fifty
  stars by December 25th.

  Collect stars by solving puzzles. Two puzzles will be made available
  on each day in the advent calendar; the second puzzle is unlocked
  when you complete the first. Each puzzle grants one star. Good luck!

  You're airdropped near Easter Bunny Headquarters in a city
  somewhere. "Near", unfortunately, is as close as you can get - the
  instructions on the Easter Bunny Recruiting Document the Elves
  intercepted start here, and nobody had time to work them out
  further.

  The Document indicates that you should start at the given
  coordinates (where you just landed) and face North. Then, follow the
  provided sequence: either turn left (L) or right (R) 90 degrees,
  then walk forward the given number of blocks, ending at a new
  intersection.

  There's no time to follow such ridiculous instructions on foot,
  though, so you take a moment and work out the destination. Given
  that you can only walk on the street grid of the city, how far is
  the shortest path to the destination?

  For example:

      Following R2, L3 leaves you 2 blocks East and 3 blocks North, or
      5 blocks away.

      R2, R2, R2 leaves you 2 blocks due South of your starting
      position, which is 2 blocks away.

      R5, L5, R5, R3 leaves you 12 blocks away.

  How many blocks away is Easter Bunny HQ?

  Then, you notice the instructions continue on the back of the
  Recruiting Document. Easter Bunny HQ is actually at the first
  location you visit twice.

  For example, if your instructions are R8, R4, R4, R8, the first
  location you visit twice is 4 blocks away, due East.

  How many blocks away is the first location you visit twice?
 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
import java.lang.Integer;
import java.lang.Math;
//import java.lang.String;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
//import java.util.regex.Pattern;
//import java.util.regex.Matcher;

public class aoc_01 {

    public static boolean __DEBUG__ = false;


    private static Dir to_right (Dir d) {
        // changes direction relative to current
        switch (d) {
        case N:
            return Dir.E;
        case E:
            return Dir.S;
        case S:
            return Dir.W;
        case W:
            return Dir.N;
        }
        return null;
    }


    private static Dir to_left (Dir d) {
        // changes direction relative to current
        switch (d) {
        case N:
            return Dir.W;
        case W:
            return Dir.S;
        case S:
            return Dir.E;
        case E:
            return Dir.N;
        }
        return null;
    }


    private static String[] read_input () {
        // reads adapted RL-input data
        // (i.e., w/o commas
        // http://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html
        Scanner sc = new Scanner (System.in);
        ArrayList <String> als = new ArrayList <String>();
        for (;sc.hasNext();) {
            String s = sc.next();
            String[] sa = s.split (" ");
            for (String t : sa)
                als.add (t.toUpperCase());
        }
        String[] t = new String [als.size()];
        return als.toArray (t);
    }


    private static Cut[] eval_dirs (String[] mvs) {
        return eval_dirs (mvs, 0, 0);
    }


    private static Cut[] eval_dirs (String[] mvs, int stx, int sty) {
        // evaluates set of movements as pinned vectors
        ArrayList <Cut> cts = new ArrayList <Cut> ();
        int x = stx, y = sty;
        Dir nd, cd = Dir.N;
        for (String m : mvs) {
            Cut t;
            nd = m.charAt(0) == 'L' ? to_left (cd) : to_right (cd);
            t = new Cut (x, y, nd, Integer.valueOf (m.substring (1)));
            cts.add (t);
            cd = nd;
            x = t.ex;
            y = t.ey;
        }
        Cut[] tc = new Cut [cts.size()];
        return cts.toArray (tc);
    }


    private static Cut straight_off (Cut[] cts) {
        // evaluates end point of path
        int x, y, i;
        for (i = x = y = 0; i < cts.length; ++i) {
            x += cts[i].ex - cts[i].sx;
            y += cts[i].ey - cts[i].sy;
        }
        return new Cut (0, 0, x, y);
    }


    private static boolean is_in (Cut ct, int x, int y) {
        int bu, bl;
        if (x == ct.sx && x == ct.ex) {
            if (ct.sy < ct.ey) {
                bu = ct.ey;
                bl = ct.sy;
            }
            else {
                bu = ct.sy;
                bl = ct.ey;
            }
            if (y <= bu && y >= bl)
                return true;
        }

        if (y == ct.sy && y == ct.ey) {
            if (ct.sx < ct.ex) {
                bu = ct.ex;
                bl = ct.sx;
            }
            else {
                bu = ct.sx;
                bl = ct.ex;
            }
            if (x <= bu && x >= bl)
                return true;
        }
        return false;
    }


    private static Cut is_crossing (Cut c0, Cut c1) {
        // evaluates crosspoing of c0 and c1
        // returns crosspoint as zero-length Cut
        // ifelse returns null
        // may crossover or overlap
        /*
        int d0x, d0y, d1x, d1y;
        int k0, b0, k1, b1;
        d0x = c0.ex - c0.sx;
        d1x = c1.ex - c1.sx;
        d0y = c0.ey - c0.sy;
        d1y = c1.ey - c1.sy;
        //if (d0x == 0 && d1x == 0 &&  || d0y == 0 && d1y == 0) return null;
        */
        int x, y;
        for (x = c0.sx, y = c0.sy; x <= c0.ex && y <= c0.ey; ) {
            if (is_in (c1, x, y))
                return new Cut (x, y, x, y);
            if (c0.sx != c0.ex)
                ++x;
            if (c0.sy != c0.ey)
                ++y;
        }
        return null;
    }


    private static Cut get_cross (Cut[] cts) {
        // find first crosspoing in given set of cuts
        // ... or returns null
        int i, j;
        for (i = 1; i < cts.length; ++i)
            for (j = 0; j < i; ++j) {
                Cut x = is_crossing (cts[i], cts[j]);
                if (x != null) {
                    if ((j+1) != i) {
                        //x.print();
                        return x;
                    }
                }
            }
        return null;
    }


    public static void main (String[] args) throws Exception {
        String[] mvs = read_input();
        Cut[] cts = eval_dirs (mvs);
        // 1st part of a puzzle
        Cut fdst = straight_off (cts);
        fdst.print();
        System.out.println (Math.abs (fdst.ex) + Math.abs (fdst.ey));
        // 2nd part of a puzzle
        Cut hq = get_cross (cts);
        if (hq != null)
            hq.print();
        else
            System.out.println ("No HQ?");
    }
}


enum Dir {
    N, E, S, W;
}


class Cut {
    int sx, sy, // start location by x:y
    //dx, dy, // length by x:y
        ex, ey; // end location by x:y

    Cut (int x0, int y0, int x1, int y1) {
        sx = x0;
        sy = y0;
        ex = x1;
        ey = y1;
    }

    Cut (int x0, int y0, Dir d, int l) {
        sx = x0;
        sy = y0;
        switch (d) {
        case N: {
            ex = sx;
            ey = y0 + l;
            break;
        }

        case S: {
            ex = sx;
            ey = y0 - l;
            break;
        }

        case E: {
            ex = x0 + l;
            ey = sy;
            break;
        }

        case W: {
            ex = x0 - l;
            ey = sy;
            break;
        }
        }
    }


    public void print () {
        System.out.printf ("%d:%d %d:%d\n", sx, sy, ex, ey);
    }
}
