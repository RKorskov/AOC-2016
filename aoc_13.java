// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-22 16:52:31 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  http://adventofcode.com/2016/day/13

  --- Day 13: A Maze of Twisty Little Cubicles ---

  You arrive at the first floor of this new building to discover a
  much less welcoming environment than the shiny atrium of the last
  one. Instead, you are in a maze of twisty little cubicles, all
  alike.

  Every location in this area is addressed by a pair of non-negative
  integers (x,y). Each such coordinate is either a wall or an open
  space. You can't move diagonally. The cube maze starts at 0,0 and
  seems to extend infinitely toward positive x and y; negative values
  are invalid, as they represent a location outside the building. You
  are in a small waiting area at 1,1.

  While it seems chaotic, a nearby morale-boosting poster explains,
  the layout is actually quite logical. You can determine whether a
  given x,y coordinate will be a wall or an open space using a simple
  system:

    Find x*x + 3*x + 2*x*y + y + y*y.

    Add the office designer's favorite number (your puzzle input).

    Find the binary representation of that sum; count the number of
    bits that are 1.

        If the number of bits that are 1 is even, it's an open space.

        If the number of bits that are 1 is odd, it's a wall.

  For example, if the office designer's favorite number were 10,
  drawing walls as # and open spaces as ., the corner of the building
  containing 0,0 would look like this:

  0123456789
0 .#.####.##
1 ..#..#...#
2 #....##...
3 ###.#.###.
4 .##..#..#.
5 ..##....#.
6 #...##.###

  Now, suppose you wanted to reach 7,4. The shortest route you could
  take is marked as O:

  0123456789
0 .#.####.##
1 .O#..#...#
2 #OOO.##...
3 ###O#.###.
4 .##OO#OO#.
5 ..##OOO.#.
6 #...##.###

  Thus, reaching 7,4 would take a minimum of 11 steps (starting from
  your current location, 1,1).

  What is the fewest number of steps required for you to reach 31,39?

  Your puzzle input is 1364.

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
import java.lang.Integer;
//import java.lang.Math;
import java.lang.String;
//import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
//import java.util.regex.Pattern;
//import java.util.regex.Matcher;

public class aoc_13 {

    public final static boolean __DEBUG__ = false;
    //public final static boolean __DEBUG__ = true;

    private static int max (final int a, final int b) { return a > b ? a : b; }

    private static int min (final int a, final int b) { return a < b ? a : b; }

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    private static boolean is_wall
        (final long x, final long y, final long dfn) {
        return !is_space (x, y, dfn);
    }


    private static boolean is_space
        (final long x, final long y, final long dfn) {
        final long z = x * (x + 3) + y * (1 + y + 2 * x) + dfn;
        int bc;
        bc = Long.bitCount (z);
        return (bc & 1) == 0 ? true : false;
    }


    private static long f (long x, long y, long dfn) {
        return x * (x + 3) + y * (1 + y + 2 * x) + dfn;
    }


    private static void maze_print
        (final int mx, final int my, final long dfn,
         final long srcx, final long srcy,
         final long dstx, final long dsty) {
        // print maze
        int i, j;
        boolean wf;
        for (j = 0; j <= my; ++j) {
            for (i = 0; i <= mx; ++i) {
                if (i == dstx && j == dsty)
                    System.out.print ('*');
                else {
                    wf = is_space ((long) i, (long) j, dfn);
                    System.out.print (wf ? ' ' : '#');
                }
            }
            System.out.println();
        }
    }


    public static void main (String[] args) throws Exception {
        final long dfn = Long.valueOf (args[0]);
        //String[] cmds = read_input(); if (cmds == null) return;
        // x:y  1:1 -> 31:39
        maze_print (80, 80, dfn, 1, 1, 31, 39);
        /*
        long x, y, n, p;
        for (y=0;y<=6;++y) {
            for (x=0;x<=10;++x) {
                p = f(x,y,10);
                System.out.printf ("%d ", Long.bitCount (p) & 1);
            }
            System.out.println();
        }
        */
    }
}

