// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-16 16:29:16 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  --- Day 9: Explosives in Cyberspace ---

  Wandering around a secure area, you come across a datalink port to a
  new part of the network. After briefly scanning it for interesting
  files, you find one file in particular that catches your
  attention. It's compressed with an experimental format, but
  fortunately, the documentation for the format is nearby.

  The format compresses a sequence of characters. Whitespace is
  ignored. To indicate that some sequence should be repeated, a marker
  is added to the file, like (10x2). To decompress this marker, take
  the subsequent 10 characters and repeat them 2 times. Then, continue
  reading the file after the repeated data. The marker itself is not
  included in the decompressed output.

  If parentheses or other characters appear within the data referenced
  by a marker, that's okay - treat it like normal data, not a marker,
  and then resume looking for markers after the decompressed section.

  For example:

    ADVENT contains no markers and decompresses to itself with no
    changes, resulting in a decompressed length of 6.

    A(1x5)BC repeats only the B a total of 5 times, becoming ABBBBBC
    for a decompressed length of 7.

    (3x3)XYZ becomes XYZXYZXYZ for a decompressed length of 9.

    A(2x2)BCD(2x2)EFG doubles the BC and EF, becoming ABCBCDEFEFG for
    a decompressed length of 11.

    (6x1)(1x3)A simply becomes (1x3)A - the (1x3) looks like a marker,
    but because it's within a data section of another marker, it is
    not treated any differently from the A that comes after it. It has
    a decompressed length of 6.

    X(8x2)(3x3)ABCY becomes X(3x3)ABC(3x3)ABCY (for a decompressed
    length of 18), because the decompressed data from the (8x2) marker
    (the (3x3)ABC) is skipped and not processed further.

  What is the decompressed length of the file (your puzzle input)?
  Don't count whitespace.

  --- Part Two ---

  Apparently, the file actually uses version two of the format.

  In version two, the only difference is that markers within
  decompressed data are decompressed. This, the documentation
  explains, provides much more substantial compression capabilities,
  allowing many-gigabyte files to be stored in only a few kilobytes.

  For example:

    (3x3)XYZ still becomes XYZXYZXYZ, as the decompressed section
    contains no markers.

    X(8x2)(3x3)ABCY becomes XABCABCABCABCABCABCY, because the
    decompressed data from the (8x2) marker is then further
    decompressed, thus triggering the (3x3) marker twice for a total
    of six ABC sequences.

    (27x12)(20x12)(13x14)(7x10)(1x12)A decompresses into a string of A
    repeated 241920 times.

    (25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN becomes
    445 characters long.

  Unfortunately, the computer you brought probably doesn't have enough
  memory to actually decompress the file; you'll have to come up with
  another way to get its decompressed length.

  What is the decompressed length of the file using this improved
  format?

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
import java.lang.Integer;
//import java.lang.Math;
import java.lang.String;
import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
//import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class aoc_09 {

    //public static boolean __DEBUG__ = false;
    public static boolean __DEBUG__ = true;

    private static String read_input () {
        StringBuilder sb = new StringBuilder ();
        for (Scanner sc = new Scanner (System.in);
             sc.hasNext();)
            for (String s: sc.next().split(" "))
                sb.append (s);
        return sb.toString();
    }


    final private static Pattern prep = Pattern.compile
        ("\\((\\d+)x(\\d+)\\)");

    private static String parse_input (final String sstr) {
        int i, j, rep, len, pos, sop;
        Matcher m;
        StringBuilder sb = new StringBuilder();
        String sr;
        m = prep.matcher (sstr);
        for (i = sop = 0; m.find (i); i = sop + len) {
            pos = m.start();
            if (pos > i) {
                sr = sstr.substring (i, pos);
                if (__DEBUG__)
                    System.out.printf ("i %d  pos %d  '%s'\n",
                                       i, pos, sr);
                sb.append (sr);
            }
            sop = m.end();
            len = Integer.parseInt (m.group (1));
            rep = Integer.parseInt (m.group (2));
            sr = sstr.substring (sop, sop + len);
            if (__DEBUG__)
                System.out.printf ("pos %d  sop %d  len %d  rep %d  '%s'\n",
                                   pos, sop, len, rep, sr);
            for (; rep > 0; --rep)
                sb.append (sr);
        }

        if (i < sstr.length()) {
            sr = sstr.substring (i);
            if (__DEBUG__) System.out.printf ("i %d  '%s'\n", i, sr);
            sb.append (sr);
        }
        return sb.toString();
    }


    private static long parse_input_1 (final String sstr) {
        long dsz, t;
        int i, j, rep, len, pos, sop;
        Matcher m;
        String sr;
        m = prep.matcher (sstr);
        for (dsz = 0, i = sop = 0; m.find (i); i = sop + len) {
            pos = m.start();
            if (pos > i) {
                dsz += (pos - i);
                if (__DEBUG__)
                    System.out.printf ("i %d  pos %d\n", i, pos);
            }
            sop = m.end();
            len = Integer.parseInt (m.group (1));
            rep = Integer.parseInt (m.group (2));
            sr = sstr.substring (sop, sop + len);
            if (__DEBUG__)
                System.out.printf ("pos %d  sop %d  len %d  rep %d  '%s'\n",
                                   pos, sop, len, rep, sr);
            t = parse_input_1 (sr);
            dsz += t * rep;
        }

        if (i < sstr.length()) {
            dsz += sstr.length() - i;
            if (__DEBUG__) System.out.printf ("  i %d\n", i);
        }
        return dsz;
    }


    public static void main (String[] args) throws Exception {
        String sstr = read_input(); if (sstr == null) return;
        String dstr = parse_input (sstr);
        long n;
        System.out.println (dstr);
        System.out.println (dstr.length());
        n = parse_input_1 (sstr);
        System.out.println (n);
    }
}
