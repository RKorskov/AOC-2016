// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2017-03-07 14:30:34 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  http://adventofcode.com/2016/day/11

  --- Day 11: Radioisotope Thermoelectric Generators ---

  hint! Ханойские башни

  You come upon a column of four floors that have been entirely sealed
  off from the rest of the building except for a small dedicated
  lobby. There are some radiation warnings and a big sign which reads
  "Radioisotope Testing Facility".

  According to the project status board, this facility is currently
  being used to experiment with Radioisotope Thermoelectric Generators
  (RTGs, or simply "generators") that are designed to be paired with
  specially-constructed microchips. Basically, an RTG is a highly
  radioactive rock that generates electricity through heat.

  The experimental RTGs have poor radiation containment, so they're
  dangerously radioactive. The chips are prototypes and don't have
  normal radiation shielding, but they do have the ability to generate
  an electromagnetic radiation shield when powered. Unfortunately,
  they can only be powered by their corresponding RTG. An RTG powering
  a microchip is still dangerous to other microchips.

  In other words, if a chip is ever left in the same area as another
  RTG, and it's not connected to its own RTG, the chip will be
  fried. Therefore, it is assumed that you will follow procedure and
  keep chips connected to their corresponding RTG when they're in the
  same room, and away from other RTGs otherwise.

  These microchips sound very interesting and useful to your current
  activities, and you'd like to try to retrieve them. The fourth floor
  of the facility has an assembling machine which can make a
  self-contained, shielded computer for you to take with you - that
  is, if you can bring it all of the RTGs and microchips.

  Within the radiation-shielded part of the facility (in which it's
  safe to have these pre-assembly RTGs), there is an elevator that can
  move between the four floors. Its capacity rating means it can carry
  at most yourself and two RTGs or microchips in any
  combination. (They're rigged to some heavy diagnostic equipment -
  the assembling machine will detach it for you.) As a security
  measure, the elevator will only function if it contains at least one
  RTG or microchip. The elevator always stops on each floor to
  recharge, and this takes long enough that the items within it and
  the items on that floor can irradiate each other. (You can prevent
  this if a Microchip and its Generator end up on the same floor in
  this way, as they can be connected while the elevator is
  recharging.)

  You make some notes of the locations of each component of interest
  (your puzzle input). Before you don a hazmat suit and start moving
  things around, you'd like to have an idea of what you need to do.

  When you enter the containment area, you and the elevator will start
  on the first floor.

  For example, suppose the isolated area has the following arrangement:

    The first floor contains a hydrogen-compatible microchip and a
    lithium-compatible microchip.

    The second floor contains a hydrogen generator.

    The third floor contains a lithium generator.

    The fourth floor contains nothing relevant.

  As a diagram (F# for a Floor number, E for Elevator, H for Hydrogen,
  L for Lithium, M for Microchip, and G for Generator), the initial
  state looks like this:

F4 .  .  .  .  .  
F3 .  .  .  LG .  
F2 .  HG .  .  .  
F1 E  .  HM .  LM 

  Then, to get everything up to the assembling machine on the fourth
  floor, the following steps could be taken:

    Bring the Hydrogen-compatible Microchip to the second floor, which
    is safe because it can get power from the Hydrogen Generator:

    F4 .  .  .  .  .  
    F3 .  .  .  LG .  
    F2 E  HG HM .  .  
    F1 .  .  .  .  LM 

    Bring both Hydrogen-related items to the third floor, which is
    safe because the Hydrogen-compatible microchip is getting power
    from its generator:

    F4 .  .  .  .  .  
    F3 E  HG HM LG .  
    F2 .  .  .  .  .  
    F1 .  .  .  .  LM 

    Leave the Hydrogen Generator on floor three, but bring the
    Hydrogen-compatible Microchip back down with you so you can still
    use the elevator:

    F4 .  .  .  .  .  
    F3 .  HG .  LG .  
    F2 E  .  HM .  .  
    F1 .  .  .  .  LM 

    At the first floor, grab the Lithium-compatible Microchip, which
    is safe because Microchips don't affect each other:

    F4 .  .  .  .  .  
    F3 .  HG .  LG .  
    F2 .  .  .  .  .  
    F1 E  .  HM .  LM 

    Bring both Microchips up one floor, where there is nothing to fry them:

    F4 .  .  .  .  .  
    F3 .  HG .  LG .  
    F2 E  .  HM .  LM 
    F1 .  .  .  .  .  

    Bring both Microchips up again to floor three, where they can be
    temporarily connected to their corresponding generators while the
    elevator recharges, preventing either of them from being fried:

    F4 .  .  .  .  .  
    F3 E  HG HM LG LM 
    F2 .  .  .  .  .  
    F1 .  .  .  .  .  

    Bring both Microchips to the fourth floor:

    F4 E  .  HM .  LM 
    F3 .  HG .  LG .  
    F2 .  .  .  .  .  
    F1 .  .  .  .  .  

    Leave the Lithium-compatible microchip on the fourth floor, but
    bring the Hydrogen-compatible one so you can still use the
    elevator; this is safe because although the Lithium Generator is
    on the destination floor, you can connect Hydrogen-compatible
    microchip to the Hydrogen Generator there:

    F4 .  .  .  .  LM 
    F3 E  HG HM LG .  
    F2 .  .  .  .  .  
    F1 .  .  .  .  .  

    Bring both Generators up to the fourth floor, which is safe
    because you can connect the Lithium-compatible Microchip to the
    Lithium Generator upon arrival:

    F4 E  HG .  LG LM 
    F3 .  .  HM .  .  
    F2 .  .  .  .  .  
    F1 .  .  .  .  .  

    Bring the Lithium Microchip with you to the third floor so you can
    use the elevator:

    F4 .  HG .  LG .  
    F3 E  .  HM .  LM 
    F2 .  .  .  .  .  
    F1 .  .  .  .  .  

    Bring both Microchips to the fourth floor:

    F4 E  HG HM LG LM 
    F3 .  .  .  .  .  
    F2 .  .  .  .  .  
    F1 .  .  .  .  .  

  In this arrangement, it takes 11 steps to collect all of the objects
  at the fourth floor for assembly. (Each elevator stop counts as one
  step, even if nothing is added to or removed from it.)

  In your situation, what is the minimum number of steps required to
  bring all of the objects to the fourth floor?

The first floor contains a polonium generator, a thulium generator, a thulium-compatible microchip, a promethium generator, a ruthenium generator, a ruthenium-compatible microchip, a cobalt generator, and a cobalt-compatible microchip.
The second floor contains a polonium-compatible microchip and a promethium-compatible microchip.
The third floor contains nothing relevant.
The fourth floor contains nothing relevant.

(g|m)(IUPAC-name);
1st hex-digit : 1 - generator, 2 - microchip, 0 - nothing
2nd hex-digit : compatibility index (eg., 11&21, 14&24, etc)
F Eq
1 10 12 22 11 13 23 14 24
2 20 21
3
4 

(example above)
1 10 11
2 20
3 21
4 

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
//import java.lang.Integer;
//import java.lang.Math;
//import java.lang.String;
//import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashSet;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class AoC_11 {

    public final static boolean __DEBUG__ = false;

    public static void main (String[] args) throws Exception {
        new AoC_11().day11 (args);
    }

    private int max (final int a, final int b) { return a > b ? a : b; }

    private int min (final int a, final int b) { return a < b ? a : b; }

    private String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        Scanner sc = new Scanner (System.in);
        sc.useDelimiter ("\n");
        for (; sc.hasNext();)
            als.add (sc.next());
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    private int[][] parse_input (final String[] afs) {
        /**
           (single long line for each floor)

           The first floor contains a polonium generator, a thulium
           generator, a thulium-compatible microchip, a promethium
           generator, a ruthenium generator, a ruthenium-compatible
           microchip, a cobalt generator, and a cobalt-compatible
           microchip.

           The second floor contains a polonium-compatible microchip
           and a promethium-compatible microchip.

           The third floor contains nothing relevant.

           The fourth floor contains nothing relevant.

           {{},{},{0},{0}}
         */
        Pattern pat_rtg = Pattern.compile ("(?i)([a-z]+) generator");
        Pattern pat_mch = Pattern.compile ("(?i)([a-z]+)-compatible microchip");
        HashSet <String> sel = new HashSet <String> ();
        HashSet <ChEl> scl = new HashSet <ChEl> ();
        ArrayList <String> alg = new ArrayList <String> (); // ChEl RTG
        ArrayList <String> alc = new ArrayList <String> (); // ChEl mCh
        for (String fs : afs) {
            // RTGs
            String[] ces = chels_findall (pat_rtg, fs);
            if (ces != null && ces.length > 0) {
                for (String s : ces)
                    sel.add (s);
            }

            // mChips
            ces = chels_findall (pat_mch, fs);
            if (ces != null && ces.length > 0) {
                for (String s : ces)
                    sel.add (s);
            }
        }
        return null;
    }


    private String[] chels_findall (final Pattern pat, final String str) {
        /**
           returns all matches of matched matcher %)

           i.e., "The second floor contains a polonium-compatible
           microchip and a promethium-compatible microchip." ~>
           {"polonium", "prometium"}
        */
        Matcher sm = pat.matcher (str);
        ArrayList <String> als = new ArrayList <String> ();
        String el;
        for (; sm.find(); ) {
            el = sm.group (1);
            als.add (el);
        }
        return als.toArray (new String [als.size()]);
    }


    private void day11 (final String[] args) {
        String[] cmds = read_input(); if (cmds == null) return;
        /*
        System.out.println ("parsing:");
        for (String s:cmds)
            System.out.println (s);
        */
        int[][] fc = parse_input (cmds); if (fc == null) return;
    }

}


interface Actor {
    /**
       floor - 0..N
       content (positive - RTG, negative - mChip) :
         0 - placeholder - nothing is here (or, in this position)
         +1..+M - RTG
         -1..-M - mChip, corresponding to RTG +1..+M)
       eg: {{-1,-2},{1},{2},{}}
       или, что тоже самое: {{-1,-2,0,0},{1,0,0,0},{2,0,0,0},{0,0,0,0}}
       ... и т.д.
     */
    public void populate (int[][] 笼); // [floor][content]
    public void move_all_to (int 从, int 到);
}


class ChEl {
    private String name;
    private int idx;
    private static int idxcount = 0;

    private ChEl (String ns) {
        if (ns != null)
            return;
        ++idxcount;
        idx = idxcount;
        name = ns;
    }

    public static ChEl new_chel (String ns) {
        if (ns == null)
            return null;
        return new ChEl (ns);
    }

    public String get_name () {
        return name;
    }

    public int get_index () {
        return idx;
    }
}
