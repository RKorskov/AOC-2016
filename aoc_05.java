// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-13 18:26:29 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  --- Day 5: How About a Nice Game of Chess? ---

  You are faced with a security door designed by Easter Bunny
  engineers that seem to have acquired most of their security
  knowledge by watching hacking movies.

  The eight-character password for the door is generated one character
  at a time by finding the MD5 hash of some Door ID (your puzzle
  input) and an increasing integer index (starting with 0).

  A hash indicates the next character in the password if its
  hexadecimal representation starts with five zeroes. If it does, the
  sixth character in the hash is the next character of the password.

  For example, if the Door ID is abc:

    The first index which produces a hash that starts with five zeroes
    is 3231929, which we find by hashing abc3231929; the sixth
    character of the hash, and thus the first character of the
    password, is 1.

    5017308 produces the next interesting hash, which starts with
    000008f82..., so the second character of the password is 8.

    The third time a hash starts with five zeroes is for abc5278568,
    discovering the character f.

  In this example, after continuing this search a total of eight
  times, the password is 18f47a30.

  Given the actual Door ID, what is the password?

  Your puzzle input is "ffykfhsq".

  --- Part Two ---

  As the door slides open, you are presented with a second door that
  uses a slightly more inspired security mechanism. Clearly
  unimpressed by the last version (in what movie is the password
  decrypted in order?!), the Easter Bunny engineers have worked out a
  better solution.

  Instead of simply filling in the password from left to right, the
  hash now also indicates the position within the password to
  fill. You still look for hashes that begin with five zeroes;
  however, now, the sixth character represents the position (0-7), and
  the seventh character is the character to put in that position.

  A hash result of 000001f means that f is the second character in the
  password. Use only the first result for each position, and ignore
  invalid positions.

  For example, if the Door ID is abc:

    The first interesting hash is from abc3231929, which produces
    0000015...; so, 5 goes in position 1: _5______.

    In the previous method, 5017308 produced an interesting hash;
    however, it is ignored, because it specifies an invalid position
    (8).

    The second interesting hash is at index 5357525, which produces
    000004e...; so, e goes in position 4: _5__e___.

  You almost choke on your popcorn as the final character falls into
  place, producing the password 05ace8e3.

  Given the actual Door ID and this new method, what is the password?
  Be extra proud of your solution if it uses a cinematic "decrypting"
  animation.


 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
import java.lang.Integer;
//import java.lang.Math;
import java.lang.String;
//import java.lang.StringBuilder;
import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class aoc_05 {

    public static boolean __DEBUG__ = false;

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        for (Scanner sc = new Scanner (System.in);
             sc.hasNext();)
            for (String s: sc.next().split(" "))
                als.add (s);
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    private static String pwgen (final String seed, final int pwlen) throws Exception {
        long p;
        String dps, s;
        byte[] ds, dd;
        MessageDigest md5 = MessageDigest.getInstance ("MD5");
        for (dps = "", p = 0; p >= 0; ++p) {
            s = String.format ("%s%d", seed, p);
            ds = s.getBytes();
            md5.update (ds);
            dd = md5.digest();

            if (__DEBUG__) {
                System.out.printf ("data, string:\n%s\n", s);
                System.out.println ("\ndata, bytes:");
                for (byte b : ds)
                    System.out.printf ("%02x ", b);
                System.out.println ("\n\ndigest:");
                for (byte b : dd)
                    System.out.printf ("%02x ", b);
                return null;
            }

            if (dd[0] == 0 && dd[1] == 0 && dd[2] >= 0 && dd[2] < 0x10) {
                System.out.printf ("%02x\n", dd[2]);
                dps = String.format ("%s%x", dps, dd[2]);
                if (dps.length() >= pwlen)
                    break;
            }

        }
        return dps;
    }


    private static String pwgen2 (final String seed, final int pwlen) throws Exception {
        // pwlen <= 15 due to algo limitation
        long p;
        int i, dc;
        String s;
        StringBuilder sb = new StringBuilder();
        byte[] ds, dd, dps = new byte [pwlen];
        for (i = 0; i < pwlen; ++i) dps[i] = -1;
        MessageDigest md5 = MessageDigest.getInstance ("MD5");
        for (dc = 0, p = 0; p >= 0; ++p) {
            s = String.format ("%s%d", seed, p);
            ds = s.getBytes();
            md5.update (ds);
            dd = md5.digest();

            if (__DEBUG__) {
                System.out.printf ("data, string:\n%s\n", s);
                System.out.println ("\ndata, bytes:");
                for (byte b : ds)
                    System.out.printf ("%02x ", b);
                System.out.println ("\n\ndigest:");
                for (byte b : dd)
                    System.out.printf ("%02x ", b);
                return null;
            }

            if (dd[0] == 0 && dd[1] == 0 && dd[2] >= 0 && dd[2] < 0x10) {
                i = dd[2];
                if (i < pwlen) {
                    //byte b = (byte) (dd[3] >> 4);
                    int q = dd[3];
                    q &= 0xF0;
                    q >>= 4;
                    byte b = (byte) q;
                    if (dps[i] == -1) {
                        dps[i] = b;
                        ++dc;
                    }
                    else
                        System.out.printf ("overlap! %02x   ", dps[i]);
                    System.out.printf ("%d %02x%02x %02x\n", i, dd[2], dd[3], b);
                    if (dc >= pwlen) break;
                }
            }

        }
        for (byte b : dps)
            sb.append (String.format ("%x", b));
        return sb.toString();
    }


    public static void main (String[] args) throws Exception {
        long p;
        int i, q;
        //String[] rdss = read_input(); if (rdss == null) return;
        System.out.println (args[0]);
        String pw1 = pwgen  (args[0], 8);
        System.out.println (pw1);
        String pw2 = pwgen2 (args[0], 8);
        System.out.println (pw2);
    }
}

