// -*- coding: utf-8; indent-tabs-mode: nil; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2016-12-14 11:06:39 korskov>

/*
  LANG=C JAVA_TOOL_OPTIONS='-Dfile.encoding=UTF8' javac Skeleton.java
  java Skeleton
*/

/*
  --- Day 6: Signals and Noise ---

  Something is jamming your communications with Santa. Fortunately,
  your signal is only partially jammed, and protocol in situations
  like this is to switch to a simple repetition code to get the
  message through.

  In this model, the same message is sent repeatedly. You've recorded
  the repeating message signal (your puzzle input), but the data seems
  quite corrupted - almost too badly to recover. Almost.

  All you need to do is figure out which character is most frequent
  for each position. For example, suppose you had recorded the
  following messages:

eedadn
drvtee
eandsr
raavrd
atevrs
tsrnev
sdttsa
rasrtv
nssdts
ntnada
svetve
tesnvt
vntsnd
vrdear
dvrsen
enarar

  The most common character in the first column is e; in the second,
  a; in the third, s, and so on. Combining these characters returns
  the error-corrected message, easter.

  Given the recording in your puzzle input, what is the
  error-corrected version of the message being sent?

  --- Part Two ---

  Of course, that would be the message - if you hadn't agreed to use a
  modified repetition code instead.

  In this modified code, the sender instead transmits what looks like
  random data, but for each character, the character they actually
  want to send is slightly less likely than the others. Even after
  signal-jamming noise, you can look at the letter distributions in
  each column and choose the least common letter to reconstruct the
  original message.

  In the above example, the least common character in the first column
  is a; in the second, d, and so on. Repeating this process for the
  remaining characters produces the original message, advent.

  Given the recording in your puzzle input and this new decoding
  methodology, what is the original message that Santa is trying to
  send?

 */

//import java.io.Console; // System.console ();
//import java.io.File;
//import java.lang.Double;
//import java.lang.Integer;
//import java.lang.Math;
//import java.lang.String;
//import java.lang.StringBuilder;
//import java.security.MessageDigest;
//import java.text.DateFormat;
import java.util.ArrayList;
//import java.util.Date;
import java.util.Scanner; // Scanner (System.in);
//import java.util.regex.Pattern;
//import java.util.regex.Matcher;

public class aoc_06 {

    public static boolean __DEBUG__ = false;

    private static String[] read_input () {
        ArrayList <String> als = new ArrayList <String> ();
        for (Scanner sc = new Scanner (System.in);
             sc.hasNext();)
            for (String s: sc.next().split(" "))
                als.add (s);
        if (als.size() > 0)
            return als.toArray (new String [als.size()]);
        return null;
    }


    private static Freq[] fanal (final String[] msgs, final int col) {
        // frequency analysis
        int i, j, m;
        char c;
        boolean fif;
        ArrayList <Freq> alf;
        Freq[] frs;
        Freq f;

        // building freq list
        alf = new ArrayList <Freq> ();
        for (m = 0; m < msgs.length; ++m) {
            c = msgs[m].charAt (col);
            // increase freq (or add new entry)
            for (fif = true, j = 0; j < alf.size(); ++j) {
                f = alf.get (j);
                if (f.ch == c) {
                    f.inc();
                    fif = false;
                    break;
                }
            }

            // new entry
            if (fif)
                alf.add (new Freq (c));
        }

        //sort!!!
        alf.sort ((f0, f1) -> {
                if (f0.fr > f1.fr)
                    return -1;
                if (f0.fr < f1.fr)
                    return 1;
                if (f0.ch > f1.ch)
                        return 1;
                if (f0.ch < f1.ch)
                    return -1;
                return 0;
            });

        frs = alf.toArray (new Freq [alf.size()]);
        return frs;
    }


    public static void main (String[] args) throws Exception {
        int i, q;
        char c;
        char[] msg1, msg2;
        Freq[] frq;
        String[] msgs = read_input(); if (msgs == null) return;

        q = msgs[0].length();
        msg1 = new char [q];
        msg2 = new char [q];
        for (i = 0; i < msgs[0].length(); ++i) {
            frq = fanal (msgs, i);
            msg1[i] = frq[0].ch;
            msg2[i] = frq[frq.length - 1].ch;
        }

        System.out.println (new String (msg1));
        System.out.println (new String (msg2));
    }
}


class Freq {
    char ch;
    int fr;

    Freq() {
        ch = 0;
        fr = 0;
    }

    Freq (char c) {
        ch = c;
        fr = 1;
    }

    Freq (char c, int f) {
        ch = c;
        fr = f;
    }

    public void inc() {
        ++fr;
    }
}
